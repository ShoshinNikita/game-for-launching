// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include "SFML\Graphics.hpp"

#include "Enemy.h"
#include "Player.h"
#include "Global.h"
#include "Magic.h"
#include "Collision.h"
#include "View.h"

using namespace std;
using namespace sf;

// Constructor
Enemy::Enemy(Object obj)
{
	width = obj.rectangle.getSize().x;
	height = obj.rectangle.getSize().y;
	rect_w = width;
	rect_h = height;

	// Try to load rects for animation
	map<string, string>::iterator it;
	string path_to_file;
	it = obj.properties.find("path");
	if (it != obj.properties.end()) {
		path_to_file = obj.properties["path"];

		load_sprite_rects(path_to_file);
	}

	rectangle = obj.rectangle;
	sprite = obj.sprite;

	rectangle.setSize(Vector2f(rect_w, rect_h));
	rectangle.setOrigin(rect_w / 2, rect_h / 2);
	sprite.setOrigin(width / 2, height / 2);

	rectangle.setPosition(rectangle.getPosition().x + rectangle.getSize().x / 2, rectangle.getPosition().y + rectangle.getSize().y / 2);
	sprite.setPosition(rectangle.getPosition());

	it = obj.properties.find("damage");
	if (it != obj.properties.end()) {
		damage = stoi(obj.properties["damage"]);
	}
	else {
		damage = 1;
	}

	it = obj.properties.find("hp");
	if (it != obj.properties.end()) {
		hp = stoi(obj.properties["hp"]);
	}
	else {
		hp = 3;
	}
	
	it = obj.properties.find("attack");
	if (it != obj.properties.end()) {
		if (obj.properties["attack"] == "melee") {
			kind_of_attack = Enemy_attacks::melee;
		}
		else if (obj.properties["attack"] == "projetile") {
			kind_of_attack = Enemy_attacks::projetile;
		}	
	}
	else {
		kind_of_attack = Enemy_attacks::projetile;
	}

	it = obj.properties.find("speed");
	if (it != obj.properties.end()) {
		speed = stoi(obj.properties["speed"]);
	}
	else {
		speed = 4;
	}
	x_direction = RIGHT;

	projectile_delay = seconds(0);
}


// Main function + AI
void Enemy::control(Player& player, RenderWindow& window)
{
	attack(player, window);
	update();
}

void Enemy::attack(Player& player, RenderWindow& window)
{
	bool can_attack = false;

	Vector2f temp_vect;
	if (rectangle.getPosition().x + rectangle.getSize().x / 2 > view.getCenter().x - view.getSize().x / 2 &&
		rectangle.getPosition().x - rectangle.getSize().x / 2 < view.getCenter().x + view.getSize().x / 2 &&
		rectangle.getPosition().y - rectangle.getSize().y / 2 > view.getCenter().y - view.getSize().y / 2 &&
		rectangle.getPosition().y + rectangle.getSize().y / 2 < view.getCenter().y + view.getSize().y / 2) {
		if (player.get_rectangle().getPosition().x >= rectangle.getPosition().x - MAX_PROJECTILE_RANGE &&
			player.get_rectangle().getPosition().x <= rectangle.getPosition().x + MAX_PROJECTILE_RANGE) {
			if (player.is_visible()) {
				can_attack = true;
			}
		}
	}

	projectile_delay = projectile_timer.getElapsedTime();

	if (kind_of_attack == Enemy_attacks::projetile && can_attack) {
		if (projectile_delay.asSeconds() > PROJECTILE_ENEMY_DELAY.asSeconds()) {

			double a = abs(rectangle.getPosition().x - player.get_rectangle().getPosition().x);
			double b = abs(rectangle.getPosition().y - player.get_rectangle().getPosition().y);
			double tangens = b / a;

			double x = 5, y;
			y = tangens * x;

			while (x*x + y*y > MAX_PROJECTILE_SPEED*MAX_PROJECTILE_SPEED) {
				x -= 0.5;
				y = tangens * x;
			}

			if (rectangle.getPosition().x - player.get_rectangle().getPosition().x > 0) {
				x *= -1;
			}
			if (rectangle.getPosition().y - player.get_rectangle().getPosition().y > 0) {
				y *= -1;
			}
			if (x != 0 && y != 0) {

				double temp_x = abs(player.get_rectangle().getPosition().x - rectangle.getPosition().x);
				double temp_y = abs(player.get_rectangle().getPosition().y - rectangle.getPosition().y);
				double temp_c = sqrt(temp_x*temp_x + temp_y*temp_y);
				attack_line.setSize(Vector2f(temp_c, 5));

				attack_line.setPosition(rectangle.getPosition());
				double corner = atan(tangens) * 180 / 3.14;

				if (player.get_rectangle().getPosition().x > rectangle.getPosition().x &&
					player.get_rectangle().getPosition().y < rectangle.getPosition().y) {
					corner = 90 - corner;
					corner -= 90;
				}
				if (player.get_rectangle().getPosition().x < rectangle.getPosition().x &&
					player.get_rectangle().getPosition().y < rectangle.getPosition().y) {
					corner += 180;
				}
				if (player.get_rectangle().getPosition().x < rectangle.getPosition().x &&
					player.get_rectangle().getPosition().y > rectangle.getPosition().y) {
					corner = 90 - corner;
					corner -= 90;
					corner -= 180;
				}
				if (player.get_rectangle().getPosition().x > rectangle.getPosition().x &&
					player.get_rectangle().getPosition().y < rectangle.getPosition().y) {
					// Nothing
				}


				attack_line.setRotation(corner);
				bool is_there_bound = false;

				for (int i = 0; i < blocks.size(); i++) {
					if (is_there_intersect(blocks[i].rectangle, attack_line)) {
						is_there_bound = true;
						break;
					}
				}

				if (!is_there_bound) {
					projectiles.push_back(Projectile ("TODO", project_sprite, rectangle, ENEMY_OWNER, Vector2f(x, y), damage));

					projectile_timer.restart();
				}

			}
		}
	}
	else if (kind_of_attack == Enemy_attacks::melee) {
		if (abs(player.get_rectangle().getPosition().x - rectangle.getPosition().x) > hitscan_sprite.getTextureRect().width) {
			if (projectile_delay.asSeconds() > PROJECTILE_ENEMY_DELAY.asSeconds() && can_attack) {
				if (player.get_rectangle().getPosition().x - rectangle.getPosition().x < 0) {
					commands.push_back(LEFT);
				}
				else {
					commands.push_back(RIGHT);
				}
			}
		}

		// Gravity
		bool intersection = false;
		for (auto i : blocks) {
			if (static_cast<int>(i.rectangle.getPosition().y - rectangle.getPosition().y - rectangle.getSize().y / 2) == 0) {
				if (rectangle.getPosition().x - rectangle.getSize().x / 2 > i.rectangle.getPosition().x &&
					rectangle.getPosition().x - rectangle.getSize().x / 2 < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					intersection = true;
					break;
				}
				if (rectangle.getPosition().x + rectangle.getSize().x / 2 > i.rectangle.getPosition().x &&
					rectangle.getPosition().x + rectangle.getSize().x / 2 < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					intersection = true;
					break;
				}
			}
		}

		if (!intersection) {
			commands.push_back(DOWN);
			on_block = false;
		}
		else if (intersection) {
			on_block = true;
		}

		Vector2i vector_move = move();

		// Setting sprites
		while (vector_move.x != 0) {
			if (vector_move.x < 0) {

				current_frame += FPS*0.0006f;
				if (current_frame >= running_sprites.size()) {
					current_frame = 0;
				}
				if (static_cast<int>(current_frame) < running_sprites.size()) {
					sprite.setTextureRect(running_sprites[static_cast<int>(current_frame)]);
				}				
				sprite.setScale(-1, 1);

				rectangle.move(-1, 0);
				vector_move.x++;
				collision(-1, 0);
			}
			else if (vector_move.x > 0) {

				current_frame += FPS*0.0006f;
				if (current_frame >= running_sprites.size()) {
					current_frame = 0;
				}
				if (static_cast<int>(current_frame) < running_sprites.size()) {
					sprite.setTextureRect(running_sprites[static_cast<int>(current_frame)]);
				}
				sprite.setScale(1, 1);

				rectangle.move(1, 0);
				vector_move.x--;
				collision(1, 0);
			}
		}
		while (vector_move.y != 0) {
			/*
			if (vector_move.y < 0) {
				if (x_direction == RIGHT) {
					sprite.setTextureRect(IntRect(width * 6, 0, width, height));
				}
				else {
					sprite.setTextureRect(IntRect(width * 7, 0, -width, height));
				}

				rectangle.move(0, -1);
				vector_move.y++;
				collision(0, -1);
			} 
			*/
			if (vector_move.y > 0) {
				
				current_frame += FPS*0.0006f;
				if (current_frame >= falling_sprites.size()) {
					current_frame = 0;
				}
				if (x_direction == RIGHT) {
					if (static_cast<int>(current_frame) < falling_sprites.size()) {
						sprite.setTextureRect(falling_sprites[static_cast<int>(current_frame)]);
					}
					sprite.setScale(1, 1);
				}
				else if (x_direction == LEFT) {
					if (static_cast<int>(current_frame) < falling_sprites.size()) {
						sprite.setTextureRect(falling_sprites[static_cast<int>(current_frame)]);
					}
					sprite.setScale(-1, 1);
				}
				
				rectangle.move(0, 1);
				vector_move.y--;
				collision(0, 1);
			}
		}

		if (is_waiting) {
			current_frame += FPS*0.0006f;
			if (current_frame >= waiting_sprites.size()) {
				current_frame = 0;
			}

			if (static_cast<int>(current_frame) < waiting_sprites.size()) {
				sprite.setTextureRect(waiting_sprites[static_cast<int>(current_frame)]);
			}

			if (x_direction == RIGHT) {
				sprite.setScale(1, 1);
			}
			else {
				sprite.setScale(-1, 1);
			}
		}

		// Change!!!

		if (abs(player.get_rectangle().getPosition().x - rectangle.getPosition().x) <= hitscan_sprite.getTextureRect().width  && 
			projectile_delay.asSeconds() > PROJECTILE_ENEMY_DELAY.asSeconds() && can_attack) {

			string temp_direction_str;
			if (player.get_rectangle().getPosition().x - rectangle.getPosition().x < 0) {
				temp_direction_str = LEFT;
			}
			else {
				temp_direction_str = RIGHT;
			}

			hitscans.push_back(Hitscan("TODO",hitscan_sprite, temp_direction_str, rectangle, ENEMY_OWNER, damage));
			
			// Change!!!
			projectile_timer.restart();
		}
	}
}

void Enemy::collision(int x, int y)
{
	for (auto i : blocks) {
		if (rectangle.getGlobalBounds().intersects(i.rectangle.getGlobalBounds())) {
			rectangle.move(-x, -y);
			break;
		}
	}
}

void Enemy::update()
{
	sprite.setPosition(rectangle.getPosition());
}


// Draw
void Enemy::draw(RenderWindow& window) {
	Text text_HP(to_string(hp), ArialRegular, 20);
	text_HP.setColor(Color::Red);

	FloatRect temp_rect = text_HP.getLocalBounds();
	text_HP.setOrigin(temp_rect.left + temp_rect.width / 2,
		temp_rect.top + temp_rect.height / 2);

	text_HP.setPosition(rectangle.getPosition().x, 
		rectangle.getPosition().y - rectangle.getSize().y / 2 - text_HP.getCharacterSize() * 1.5);

	window.draw(sprite);
	window.draw(text_HP);
}


// Change enemy's stats
void Enemy::damage_enemy(int getting_damage) {
	hp -= getting_damage;
}

void Enemy::set_hp(int saved_hp)
{
	hp = saved_hp;
}

void Enemy::set_position(Vector2f pos) {
	rectangle.setPosition(pos);
}


// Return enemy's status or enemy's stats
bool Enemy::is_alive()
{
	if (hp <= 0) {
		return false;
	}
	else {
		return true;
	}
}

int Enemy::get_hp()
{
	return hp;
}

RectangleShape Enemy::get_rectangle() {
	return rectangle;
}

RectangleShape Enemy::get_attack_line() {
	return attack_line;
}