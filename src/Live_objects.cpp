// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "SFML\Graphics.hpp"

#include <sstream>
#include <fstream>
#include <iostream>

#include "Global.h"
#include "Live_objects.h"

using namespace std;
using namespace sf;

Vector2i Live_objects::move() {
	int x = 0, y = 0;

	if (commands.size() == 0) {
		is_waiting = true;
	}
	else {
		is_waiting = false;
	}

	for (auto i : commands) {
		if (i == UP) {
			y -= gravity;
		}
		else if (i == RIGHT) {
			x += speed;
		}
		else if (i == LEFT) {
			x -= speed;
		}
		else if (i == DOWN) {
			y += gravity;
		}
	}

	if (x > 0) {
		x_direction = RIGHT;
	}
	else if (x < 0) {
		x_direction = LEFT;
	}


	if (y < 0) {
		y_direction = UP;
	}
	else if (y > 0) {
		y_direction = DOWN;
	}

	if (x == 0 && y == 0) {
		is_waiting = true;
	}

	commands.clear();

	return Vector2i(x, y);
}

void Live_objects::load_sprite_rects(string path)
{
	width = 0;
	height = 0;
	rect_h = 0;
	rect_w = 0;

	ifstream hero_settings_file(path);
	if (!hero_settings_file.good()) {
		cout << "Bad path to file with rectangles for sprites\n\n";
	}
	string temp_str;
	while (hero_settings_file.peek() != EOF) {
		getline(hero_settings_file, temp_str);
		if (temp_str[0] != '#') {
			stringstream s_string(temp_str);
			string ch; // characteristic
			s_string >> ch;
			if (ch == "WH:") {
				s_string >> width >> height;
			}
			else if (ch == "Rect_WH:") {
				s_string >> rect_w >> rect_h;
			}
			else if (ch == "Running:") {
				int x, y;
				while (s_string >> x >> y) {
					IntRect temp_rect;
					temp_rect.top = y;
					temp_rect.left = x;
					temp_rect.height = height;
					temp_rect.width = width;
					running_sprites.push_back(temp_rect);
				}
			}
			else if (ch == "Waiting:") {
				int x, y;
				while (s_string >> x >> y) {
					IntRect temp_rect;
					temp_rect.top = y;
					temp_rect.left = x;
					temp_rect.height = height;
					temp_rect.width = width;
					waiting_sprites.push_back(temp_rect);
				}
			}
			else if (ch == "Death:") {
				int x, y;
				while (s_string >> x >> y) {
					IntRect temp_rect;
					temp_rect.top = y;
					temp_rect.left = x;
					temp_rect.height = height;
					temp_rect.width = width;
					death_sprites.push_back(temp_rect);
				}
			}
			else if (ch == "Jumping:") {
				int x, y;
				while (s_string >> x >> y) {
					IntRect temp_rect;
					temp_rect.top = y;
					temp_rect.left = x;
					temp_rect.height = height;
					temp_rect.width = width;
					jumping_sprites.push_back(temp_rect);
				}
			}
			else if (ch == "Falling:") {
				int x, y;
				while (s_string >> x >> y) {
					IntRect temp_rect;
					temp_rect.top = y;
					temp_rect.left = x;
					temp_rect.height = height;
					temp_rect.width = width;
					falling_sprites.push_back(temp_rect);
				}
			}
		}
	}

	// Checking
	if (rect_w == 0) {
		rect_w = width;
	}
	if (rect_h == 0) {
		rect_h = height;
	}
}

