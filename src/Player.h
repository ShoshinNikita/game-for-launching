#ifndef PLAEYR_H
#define PLAEYR_H

#include "SFML\Graphics.hpp"

#include <string>

#include "Live_objects.h"

using namespace std;
using namespace sf;

class Player : public Live_objects
{
	// Jumps and falls
	bool jumping;
	float time_in_air;
	/*
	// For double jump
	bool second_jump;
	int double_jump;
	*/

	bool on_half_block;
	bool on_stairs;
	bool visible;
	
	int chosen_spell;
	

	// For delay between getting damage
	Time immortal_time;
	Clock immortal_timer;

	// For delay between shoots
	Time projectile_delay, hitscan_delay;
	Clock projectile_timer, hitscan_timer;

	// For mana recovery
	Time mana_recovery_time;
	Clock mana_recovey_timer;

	// For invisibility
	Time visible_time;
	Clock visible_timer;


	// Local functions
	void command_treatment();
	void collision(int, int);
	void ineractive_with_map();
	void update();

public:	
	
	/// <summary> Sprite</summary>
	Player();

	/// <summary> Interface </summary>
	void control();
	
	/// <summary> Set position of sprite like position of rectangle </summary>
	void damage_hero(int);
	
	
	void load_stats_from_save();
	void recovery();
	void set_chosen_spell(int);
	/// <summary> For setting start position of hero on new levels </summary>
	void set_position(Vector2f);
	
	int get_hp();
	int get_mana();
	string get_x_direction();
	string get_y_direction();
	RectangleShape get_rectangle();
	Sprite get_sprite();
	bool is_on_finish();
	bool is_alive();
	// TODO: is this necessary?
	bool is_visible();
};

#endif