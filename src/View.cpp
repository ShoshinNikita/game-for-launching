// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <iostream>

#include "SFML\Graphics.hpp"

#include "View.h"
#include "Map.h"
#include "Global.h"

View view;

void set_camera_position(Vector2f player_position, RenderWindow& window, Level& level) {
	float temp_x = player_position.x;
	float temp_y = player_position.y;

	if (temp_x < view.getSize().x / 2) { 
		temp_x = view.getSize().x / 2;
	}
	else if (temp_x > level.get_level_size().x * TILE_SIZE.x - view.getSize().x / 2) {
		temp_x = level.get_level_size().x * TILE_SIZE.x - view.getSize().x / 2;
	}

	if (temp_y < view.getSize().y / 2) {
		temp_y = view.getSize().y / 2;
	}
	else if (temp_y > level.get_level_size().y * TILE_SIZE.y - view.getSize().y / 2) {
		temp_y = level.get_level_size().y * TILE_SIZE.y - view.getSize().y / 2;
	}

	view.setCenter(temp_x, temp_y);
}
