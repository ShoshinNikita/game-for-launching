// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "SFML\Graphics.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <iterator>
#include <fstream>
#include <sstream>

#include "Global.h"
#include "Player.h"
#include "Magic.h"
#include "Map.h"

using namespace std;
using namespace sf;

// Constructor
Player::Player()
{
	hp = MAX_HP;
	mana = MAX_MANA;

	width = hero_sprite.getTextureRect().width;
	height = hero_sprite.getTextureRect().height;
	rect_w = width;
	rect_h = height;

	// Load rects for animation
	load_sprite_rects("resources\\textures\\character\\settings.txt");


	rectangle.setPosition(0, 0);
	rectangle.setSize(Vector2f(rect_w, rect_h));
	rectangle.setOrigin(rect_w / 2, rect_h / 2);
	sprite = hero_sprite;
	sprite.setOrigin(width / 2, height / 2);

	speed = SPEED;
	boost = 1.0;

	x_direction = RIGHT;

	current_frame = 0;

	jumping = false;
	time_in_air = 0;

	on_block = true;
	on_half_block = false;
	on_stairs = false;
	visible = true;
	is_waiting = true;

	chosen_spell = projectile_attack;

	projectile_delay = seconds(0);
	hitscan_delay = seconds(0);
	mana_recovery_time = seconds(0);
	immortal_time = seconds(1);

	color = Color::White;
}


// Main functions
void Player::control()
{
	command_treatment();
	ineractive_with_map();
	update();
}

void Player::command_treatment()
{
	projectile_delay = projectile_timer.getElapsedTime();
	hitscan_delay = hitscan_timer.getElapsedTime();

	// Block with treatment of commands

	// TODO (canceling)
	if (Keyboard::isKeyPressed(Keyboard::Z) && mana > 0) {
		if (visible && visible_timer.getElapsedTime() > seconds(0.5)) {
			visible = false;
			visible_timer.restart();
		}
		else if (visible_timer.getElapsedTime() > seconds(0.5)) {
			visible = true;
			visible_timer.restart();

		}
	}

	if (Keyboard::isKeyPressed(Keyboard::F) && chosen_spell == Player_attacks::projectile_attack &&
		projectile_delay.asSeconds() >= PROJECTILE_PLAYER_DELAY.asSeconds() &&
		mana - PROJECTILE_MANA_COST >= 0) {
		projectiles.push_back(Projectile("TODO", project_sprite, x_direction, rectangle, PLAYER_OWNER));

		mana -= PROJECTILE_MANA_COST;
		// TODO (?)
		mana_recovey_timer.restart();

		projectile_timer.restart();
	}
	// TODO
	if (Keyboard::isKeyPressed(Keyboard::V) && /*chosen_spell == Player_attacks::hitscan_attack &&*/
		hitscan_delay.asSeconds() >= HITSCAN_PLAYER_DELAY.asSeconds() && mana - HITSCAN_MANA_COST >= 0) {
		// TODO
		hitscans.push_back(Hitscan("TODO", hitscan_sprite, x_direction, rectangle, PLAYER_OWNER));

		mana -= HITSCAN_MANA_COST;
		mana_recovey_timer.restart();

		hitscan_timer.restart();
	}

	if (Keyboard::isKeyPressed(Keyboard::D)) {
		commands.push_back(RIGHT);
	}
	if (Keyboard::isKeyPressed(Keyboard::A)) {
		commands.push_back(LEFT);
	}

	if (Keyboard::isKeyPressed(Keyboard::W) && on_stairs) {
		commands.push_back(UP);
	}
	if (Keyboard::isKeyPressed(Keyboard::S)) {
		if (on_stairs) {
			commands.push_back(DOWN);
		}
		else if (on_half_block) {
			rectangle.move(0, 2);
		}
		
	}

	if (Keyboard::isKeyPressed(Keyboard::Space)) {
		if ((on_block || on_half_block) && !jumping && !on_stairs) {
			commands.push_back(UP);
			on_block = false;
			jumping = true;
		}
	}


	// Block with falling and jumping
	if (!on_stairs) {
		// Jumping
		if (jumping && time_in_air < MAX_TIME_IN_AIR) {
			time_in_air += gravity;
			commands.push_back(UP);
		}
		else if (time_in_air >= MAX_TIME_IN_AIR) {
			jumping = false;
			commands.push_back(DOWN);
			time_in_air = 0;
		}

		// Collision with blocks
		for (auto i : blocks) {
			if (static_cast<int>(i.rectangle.getPosition().y - rectangle.getPosition().y - rectangle.getSize().y / 2) == 0) {
				if (rectangle.getPosition().x - rectangle.getSize().x / 2 > i.rectangle.getPosition().x &&
					rectangle.getPosition().x - rectangle.getSize().x / 2 < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					on_block = true;
					break;
				}
				if (rectangle.getPosition().x + rectangle.getSize().x / 2 > i.rectangle.getPosition().x &&
					rectangle.getPosition().x + rectangle.getSize().x / 2 < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					on_block = true;
					break;
				}
				if (rectangle.getPosition().x > i.rectangle.getPosition().x &&
					rectangle.getPosition().x < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					on_block = true;
					break;
				}
			}
			else {
				on_block = false;
			}
		}
		for (auto i : half_blocks) {
			if (static_cast<int>(i.rectangle.getPosition().y - rectangle.getPosition().y - rectangle.getSize().y / 2) == 0) {
				if (rectangle.getPosition().x - rectangle.getSize().x / 2 > i.rectangle.getPosition().x &&
					rectangle.getPosition().x - rectangle.getSize().x / 2 < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					on_half_block = true;
					break;
				}
				if (rectangle.getPosition().x + rectangle.getSize().x / 2 > i.rectangle.getPosition().x &&
					rectangle.getPosition().x + rectangle.getSize().x / 2 < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					on_half_block = true;
					break;
				}
				if (rectangle.getPosition().x > i.rectangle.getPosition().x &&
					rectangle.getPosition().x < i.rectangle.getPosition().x + i.rectangle.getSize().x) {
					on_half_block = true;
					break;
				}
			}
			else {
				on_half_block = false;
			}
		}
		if (!on_block && !on_half_block && !jumping) {
			commands.push_back(DOWN);
		}
	}

	Vector2i vector_move = move();

	// Setting sprites and move
	while (vector_move.x != 0) {
		if (vector_move.x < 0) {

			current_frame += FPS*0.0006f;
			if (current_frame >= running_sprites.size()) { 
				current_frame = 0;
			}
			sprite.setTextureRect(running_sprites[static_cast<int>(current_frame)]);
			sprite.setScale(-1, 1);

			rectangle.move(-1, 0);
			vector_move.x++;
			collision(-1, 0);
		}
		else if (vector_move.x > 0) {

			current_frame += FPS*0.0006f;
			if (current_frame >= running_sprites.size()) {
				current_frame = 0;
			}
			sprite.setTextureRect(running_sprites[static_cast<int>(current_frame)]);
			sprite.setScale(1, 1);

			rectangle.move(1, 0);
			vector_move.x--;
			collision(1, 0);
		}
	}

	while (vector_move.y != 0) {
		if (vector_move.y > 0) {
			current_frame += FPS*0.0006f;
			if (current_frame >= falling_sprites.size()) {
				current_frame = 0;
			}

			if (x_direction == RIGHT) {
				sprite.setTextureRect(falling_sprites[static_cast<int>(current_frame)]);
				sprite.setScale(1, 1);
			}
			else {
				sprite.setTextureRect(falling_sprites[static_cast<int>(current_frame)]);
				sprite.setScale(-1, 1);
			}

			rectangle.move(0, 1);
			vector_move.y--;
			collision(0, 1);
		}
		else if (vector_move.y < 0) {
			current_frame += FPS*0.0006f;
			if (current_frame >= jumping_sprites.size()) {
				current_frame = 0;
			}

			if (x_direction == RIGHT) {
				sprite.setTextureRect(jumping_sprites[static_cast<int>(current_frame)]);
				sprite.setScale(1, 1);
			}
			else if (x_direction == LEFT) {
				sprite.setTextureRect(jumping_sprites[static_cast<int>(current_frame)]);
				sprite.setScale(-1, 1);
			}

			rectangle.move(0, -1);
			vector_move.y++;
			collision(0, -1);
		}
	}

	if (is_waiting) {
		current_frame += FPS*0.0006f;
		if (current_frame >= waiting_sprites.size()) {
			current_frame = 0;
		}

		if (x_direction == RIGHT) {
			sprite.setTextureRect(waiting_sprites[static_cast<int>(current_frame)]);
			sprite.setScale(1, 1);
		}
		else {
			sprite.setTextureRect(waiting_sprites[static_cast<int>(current_frame)]);
			sprite.setScale(-1, 1);
		}
	}
}

void Player::collision(int x, int y)
{
	for (auto i : blocks) {
		if (rectangle.getGlobalBounds().intersects(i.rectangle.getGlobalBounds())) {
			rectangle.move(-x, -y);
			break;
		}
	}
	for (auto i : half_blocks) {
		if (rectangle.getGlobalBounds().intersects(i.rectangle.getGlobalBounds())) {
 			if (static_cast<int>(i.rectangle.getPosition().y + 1 - rectangle.getPosition().y - rectangle.getSize().y / 2) == 0 
				&& y > 0) {
				rectangle.move(0, -y);
				break;
			}
		}
	}
}

void Player::ineractive_with_map() 
{
	// Thorns
	for (auto i : thorns) {
		if (rectangle.getGlobalBounds().intersects(i.rectangle.getGlobalBounds())) {
			damage_hero(1);
		}
	}

	// Stairs
	// TODO
	on_stairs = false;
	for (auto i : stairs) {
		// -1 and +1 for fixing 
		if (rectangle.getPosition().x + 1 > i.rectangle.getPosition().x &&
			rectangle.getPosition().x - 1 < i.rectangle.getPosition().x + i.rectangle.getSize().x &&
			rectangle.getPosition().y + rectangle.getSize().y / 2  > i.rectangle.getPosition().y &&
			rectangle.getPosition().y < i.rectangle.getPosition().y + i.rectangle.getSize().y) {
			on_stairs = true;
			break;
		}
	}

	// Messages
	for (int i = 0; i < messages_vector.size(); i++) {
		if (rectangle.getGlobalBounds().intersects(messages_vector[i].get_rectangle().getGlobalBounds())){
			if (Keyboard::isKeyPressed(Keyboard::X)) {
				messages_vector[i].set_visible(true);
			}
		}
		else {
			if (!rectangle.getGlobalBounds().intersects(messages_vector[i].get_rectangle().getGlobalBounds())) {
				messages_vector[i].set_visible(false);
			}
		}
	}
}

void Player::damage_hero(int value_of_damage)
{
	immortal_time = immortal_timer.getElapsedTime();

	if (immortal_time.asSeconds() >= MAX_IMMORTAL_TIME.asSeconds()) {
		hp -= value_of_damage;
		immortal_timer.restart();
		color = Color::Red;
	}
}

void Player::update()
{
	// TODO: bool is_using_spells
	// Recovery of mana
	if (mana < MAX_MANA && /*This -> */ visible) {
		mana_recovery_time = mana_recovey_timer.getElapsedTime();
		if (mana_recovery_time.asSeconds() >=/*This ->*/ seconds(1).asSeconds()) {
			mana++;
			mana_recovey_timer.restart();
		}
	}

	if (!visible) {
		visible_time = visible_timer.getElapsedTime();
		if (mana < 1) {
			visible = true;
		}
		else if (visible_time >= seconds(1)) {
			mana--;
			visible_timer.restart();
		}
	}

	// Color
	immortal_time = immortal_timer.getElapsedTime();
	if (immortal_time.asSeconds() >= MAX_IMMORTAL_TIME.asSeconds()) {
		color = Color::White;
	}
	sprite.setColor(color);
	if (!visible) {
		sprite.setColor(Color::Color(255, 255, 255, 30));
	}

	// Set sprite position
	sprite.setPosition(rectangle.getPosition());
}


// Change player's stats
void Player::load_stats_from_save() {
	hp = saved_hp;
	mana = saved_mana;
	color = Color::White;
}

void Player::recovery()
{

	jumping = false;
	time_in_air = 0;

	on_block = true;
	on_half_block = false;
	on_stairs = false;
	visible = true;
	is_waiting = true;

	projectile_delay = seconds(0);
	hitscan_delay = seconds(0);
	mana_recovery_time = seconds(0);
	immortal_time = seconds(1);

	color = Color::White;

	mana = MAX_MANA;
	hp = MAX_HP;
}

void Player::set_chosen_spell(int spell) {
	chosen_spell = spell;
}

void Player::set_position(Vector2f pos)
{
	rectangle.setPosition(pos.x, pos.y);
}


// Return player's status or player's stats
int Player::get_hp() {
	return hp;
}

int Player::get_mana()
{
	return mana;
}

string Player::get_x_direction()
{
	return x_direction;
}

string Player::get_y_direction()
{
	return y_direction;
}

RectangleShape Player::get_rectangle()
{
	return rectangle;
}

Sprite Player::get_sprite()
{
	return sprite;
}

bool Player::is_on_finish()
{
	if (rectangle.getGlobalBounds().intersects(finish.rectangle.getGlobalBounds())) {
		return true;
	}
	else {
		return false;
	}
}

bool Player::is_alive() {
	if (hp > 0) {
		return true;
	}
	else {
		return false;
	}
}

bool Player::is_visible() {
	return visible;
}