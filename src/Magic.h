#ifndef MAGIC_H
#define MAGIC_H

#include "SFML\Graphics.hpp"

#include <string>
#include <vector>

#include "Player.h"

using namespace std;
using namespace sf;

struct delay_between_dmg
{
	Time time;
	Clock timer;
	// if player's magic -> name == number of enemy; if enemy's magic -> name == player
	string target;
	// for damaging in first time
	bool is_first_time;
};

class Attack_skills
{
protected:
	string name;
	vector<IntRect> sprites_rectangles;

	string x_direction;

	float range;
	int damage;

	RectangleShape rectangle;
	Texture texture;
	Sprite sprite;

	string owner;

	bool is_drawable;

	int mana_cost;

	vector<delay_between_dmg> targets;

public:

	void draw(RenderWindow&);
	
	bool can_be_damaged(string, Time);
		
	int get_mana_cost();
	
	float get_range();
	Sprite get_sprite();
	string get_owner();
};


// Ice_arrow
class Projectile : public Attack_skills
{
private:
	Vector2f vector_direction;
	float done_distance;

public:
	/// <summary> For player: sprite, direction, rectangel, owner </summary>
	Projectile(string, Sprite, string, RectangleShape&, string);

	/// <summary> For enemy: sprite, rectangel, owner,, vector for moving, enemy's damage </summary>
	Projectile(string, Sprite, RectangleShape&, string, Vector2f, int);

	void move(Player&);

	void draw(RenderWindow&);
};


// Water_whip
class Hitscan : public Attack_skills
{
private:
	Time live_time;
	Clock live_timer;

	RectangleShape host_rectangle;
public:

	/// <summary> For player and enemy: name, sprite, direction, rectangle, owner </summary>
	Hitscan(string, Sprite, string, RectangleShape&, string);
	
	/// <summary> For player and enemy: name, sprite, direction, rectangle, owner, dmg </summary>
	Hitscan(string, Sprite, string, RectangleShape&, string, int);

	void attack(Player&);
	void draw(RenderWindow&);

	/// <summary> For player (can change dirextion) </summary>
	void move(Player&);

	/// <summary> For enemy (can't change dirextion) </summary>
	void move(RectangleShape&);
};


// Tsunami
class Super_attack : public Attack_skills
{
	vector<string> damaged_enemies;

public:
	/// <summary> Sprite, owner </summary>
	Super_attack(Sprite, string);

	/// <summary> Move and attack </summary>
	void move();
};

#endif // MAGIC_H

