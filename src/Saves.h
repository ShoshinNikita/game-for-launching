#ifndef SAVES_H
#define SAVES_H

#include "Player.h"
#include"Map.h"

void load_save_file();
void create_new_save_file();
void save_game(Level&, Player&);
void load_save_from_RAM(Level&, Player&);

#endif // !SAVES_H

