// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "SFML\Graphics.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include "Magic.h"
#include "Player.h"
#include "Map.h"
#include "View.h"
#include "Global.h"
#include "Menu.h"
#include "Saves.h"
#include "Collision.h"

using namespace std;
using namespace sf;

void load_textures();
void action(RenderWindow&, Level&);

bool full_screen;

int main() {
	Level level;
	RenderWindow window;

	locale::global(locale("rus"));

	// Load sprites and textures
	try {
		load_textures();
	}
	catch (exception& e) {
		cout << e.what() << endl << "Press Enter...";
		cin.ignore();
		return 0;
	}

	// Load Font
	if (!ArialRegular.loadFromFile("resources\\font\\ArialRegular.ttf")) {
		cout << "Error: font is not loaded\nPress Enter";
		cin.ignore();
		return 0;
	}

	level.set_config("paths.txt");

	ifstream input_dialogs("dialogs.txt");
	while (input_dialogs.good()) {
		string temp_str;
		getline(input_dialogs, temp_str);
		dialogs.push_back(temp_str);
	}
	input_dialogs.close();


	// Create window
	int window_width = 800, window_height = 600;
	full_screen = false;

	ifstream input("config.txt");
	while(input.peek() != EOF) {
		string temp_str;
		getline(input, temp_str);
		if (temp_str[0] != '#') {
			istringstream s_sting(temp_str);
			s_sting >> window_width >> window_height >> full_screen;
		}
	}
	input.close();

	if (full_screen) {
		window.create(VideoMode(window_width, window_height), "Game", Style::Fullscreen);
	}
	else {
		window.create(VideoMode(window_width, window_height), "Game");
	}

	// Over game loop
	while (window.isOpen()) {
		// Settings for main menu
		view.setSize(window.getSize().x, window.getSize().y);
		view.setCenter(view.getSize().x / 2, view.getSize().y / 2);
		window.setView(view);

		// Show menu
		int command = show_menu_window(window, full_screen);
		if (command == exit_from_game) {
			window.close();
		}
		if (command == new_game) {
			create_new_save_file();
		}
		else if (command == continue_game) {
			load_save_file();
		}

		view.setSize(visible_area);
		window.setVerticalSyncEnabled(true);
		window.setFramerateLimit(FPS);
		
		// Game loop
		action(window, level);
	}

	return 0;
}


void action(RenderWindow& window, Level& level)
{
	Player player;

	// Load level and set player's stats
	load_save_from_RAM(level, player);
	
	bool focus_on_window = true;
	bool game_end = false;
	bool return_to_main_menu = false;

	// Game loop
	while (window.isOpen()) {
		Event event;

		// Event
		while (window.pollEvent(event)) {
			if (event.type == Event::Closed) {
				window.close();
			}
			if (game_end && event.key.code == Keyboard::Escape) {
				return_to_main_menu = true;
			}
			else if (event.key.code == Keyboard::Escape) {
				view.setCenter(window.getSize().x / 2, window.getSize().y / 2);
				window.setView(view);

				if (!show_pause_menu(window, full_screen, level, player)) {
					return_to_main_menu = true;
				}

				view.setSize(visible_area);
				window.setVerticalSyncEnabled(true);
				window.setFramerateLimit(FPS);
			}

			if (event.type == Event::KeyPressed && event.key.code == Keyboard::Tilde) {
				develop_menu(window, level, player);
			}

			if (event.type == Event::KeyPressed && event.key.code == Keyboard::R) {
				load_save_from_RAM(level, player);
			}
			if (event.type == Event::LostFocus) {
				focus_on_window = false;
			}
			if (event.type == Event::GainedFocus) {
				focus_on_window = true;
			}
		}

		if (return_to_main_menu) {
			break;
		}

		if (focus_on_window && !game_end) {
			// Hero
			if (player.is_alive()) {
				player.control();
				set_camera_position(player.get_rectangle().getPosition(), window, level);
			}

			// Projectiles attack
			for (int i = 0; i < projectiles.size(); i++) {
				projectiles[i].move(player);
			}

			// Hitscans move and attack
			for (int i = 0; i < hitscans.size(); i++) {
				hitscans[i].attack(player);

				if (hitscans[i].get_owner() == PLAYER_OWNER) {
					hitscans[i].move(player);
				}
				else {
					hitscans[i].move(player.get_rectangle());
				}
			}

			// Enemies attack
			for (int i = 0; i < enemies.size(); i++) {
				if (enemies[i].is_alive()) {
					enemies[i].control(player, window);
				}
			}

			// View
			window.setView(view);
			level.move_background(view);

			// Draw level
			window.clear();
			level.draw(window, player);
			window.display();


			// load_stats_from_save player
			if (!player.is_alive()) {
				load_save_from_RAM(level, player);
			}

			// Next level
			if (developer_number_of_level != -1) {
				level.load_level(developer_number_of_level);
				player.recovery();
				player.set_position(spawn.rectangle.getPosition());
				developer_number_of_level = -1;
			}

			if (player.is_on_finish()) {
				if (level.is_end()) {
					game_end = true;
				}
				else {
					level.next_level();
					player.recovery();
					player.set_position(spawn.rectangle.getPosition());
					save_game(level, player);
				}
			}
		}
		else if (game_end) {
			Text congratulations("Congratulations!", ArialRegular, 70);
			FloatRect temp_rect = congratulations.getLocalBounds();
			congratulations.setOrigin(temp_rect.left + temp_rect.width / 2, temp_rect.top + temp_rect.height / 2);
			congratulations.setPosition(view.getCenter());

			Text message("You won, maybe... Please, send your feedback in VK", ArialRegular, 40);
			temp_rect = message.getLocalBounds();
			message.setOrigin(temp_rect.left + temp_rect.width / 2, temp_rect.top + temp_rect.height / 2);
			message.setPosition(view.getCenter());
			message.setPosition(view.getCenter().x, view.getCenter().y + congratulations.getCharacterSize() + 10);

			window.clear();
			window.draw(congratulations);
			window.draw(message);
			window.display();
		}
	}

}

void load_textures()
{
	// Attacks
	if (!project_texture.loadFromFile("resources\\textures\\magic\\fireball.png")) {
		throw exception("Error: texture of projectile is not loaded");
	}
	project_sprite.setTexture(project_texture);
	project_sprite.setTextureRect(IntRect(0, 0, project_texture.getSize().x, project_texture.getSize().y));

	if (!hitscan_texture.loadFromFile("resources\\textures\\magic\\hitscan.png")) {
		throw exception("Error: texture of hitscan is not loaded");
	}
	hitscan_sprite.setTexture(hitscan_texture);
	hitscan_sprite.setTextureRect(IntRect(0, 0, hitscan_texture.getSize().x, hitscan_texture.getSize().y));

	// Character
	if (!hero_texture.loadFromFile("resources\\textures\\character\\man.png")) {
		throw exception("Error: hero_texture is not loaded");
	}
	hero_sprite.setTexture(hero_texture);
	hero_sprite.setTextureRect(IntRect(0, 0, 92, 100));

	// Interface
	if (!full_health_texture.loadFromFile("resources\\textures\\interface\\full_health.png")) {
		throw exception("Error: texture of full_health is not loaded");
	}
	full_health_sprite.setTexture(full_health_texture);
	full_health_sprite.setTextureRect(IntRect(0, 0, full_health_texture.getSize().x, full_health_texture.getSize().y));

	if (!empty_health_texture.loadFromFile("resources\\textures\\interface\\empty_health.png")) {
		throw exception("Error: texture of empty_health is not loaded");
	}
	empty_health_sprite.setTexture(empty_health_texture);
	empty_health_sprite.setTextureRect(IntRect(0, 0, empty_health_texture.getSize().x, empty_health_texture.getSize().y));

	if (!full_mana_texture.loadFromFile("resources\\textures\\interface\\full_mana.png")) {
		throw exception("Error: texture of full_mana is not loaded");
	}
	full_mana_sprite.setTexture(full_mana_texture);
	full_mana_sprite.setTextureRect(IntRect(0, 0, full_mana_texture.getSize().x, full_mana_texture.getSize().y));

	if (!empty_mana_texture.loadFromFile("resources\\textures\\interface\\empty_mana.png")) {
		throw exception("Error: texture of empty_mana is not loaded");
	}
	empty_mana_sprite.setTexture(empty_mana_texture);
	empty_mana_sprite.setTextureRect(IntRect(0, 0, empty_mana_texture.getSize().x, empty_mana_texture.getSize().y));

	// Main menu
	if (!background_texture.loadFromFile("resources\\textures\\menu\\background.png")) {
		throw exception("Error: texture of background is not loaded");
	}
	background_sprite.setTexture(background_texture);

	if (!continue_texture.loadFromFile("resources\\textures\\menu\\continue.png")) {
		throw exception("Error: texture of continue button is not loaded");
	}
	continue_sprite.setTexture(continue_texture);

	if (!new_game_texture.loadFromFile("resources\\textures\\menu\\new_game.png")) {
		throw exception("Error: texture of new_game button is not loaded");
	}
	new_game_sprite.setTexture(new_game_texture);

	if (!settings_texture.loadFromFile("resources\\textures\\menu\\settings.png")) {
		throw exception("Error: texture of settings button is not loaded");
	}
	settings_sprite.setTexture(settings_texture);

	if (!exit_texture.loadFromFile("resources\\textures\\menu\\exit.png")) {
		throw exception("Error: texture of exit button is not loaded");
	}
	exit_sprite.setTexture(exit_texture);

	// Pause menu
	if (!pause_texture.loadFromFile("resources\\textures\\menu\\pause.png")) {
		throw exception("Error: texture of pause button is not loaded");
	}
	pause_sprite.setTexture(pause_texture);

	if (!save_texture.loadFromFile("resources\\textures\\menu\\save.png")) {
		throw exception("Error: texture of save button is not loaded");
	}
	save_sprite.setTexture(save_texture);
	
	// Settings menu
	if (!resolution_texture.loadFromFile("resources\\textures\\menu\\screen_resolution.png")) {
		throw exception("Error: texture of screen_resolution button is not loaded");
	}
	resolution_sprite.setTexture(resolution_texture);

	if (!_1920x1080_texture.loadFromFile("resources\\textures\\menu\\resolutions\\1920x1080.png")) {
		throw exception("Error: texture of 1920x1080 button is not loaded");
	}
	_1920x1080_sprite.setTexture(_1920x1080_texture);

	if (!_1600x900_texture.loadFromFile("resources\\textures\\menu\\resolutions\\1600x900.png")) {
		throw exception("Error: texture of 1600x900 button is not loaded");
	}
	_1600x900_sprite.setTexture(_1600x900_texture);

	if (!_1366x768_texture.loadFromFile("resources\\textures\\menu\\resolutions\\1366x768.png")) {
		throw exception("Error: texture of 1366x768 button is not loaded");
	}
	_1366x768_sprite.setTexture(_1366x768_texture);

	if (!_1280x720_texture.loadFromFile("resources\\textures\\menu\\resolutions\\1280x720.png")) {
		throw exception("Error: texture of 1280x720 button is not loaded");
	}
	_1280x720_sprite.setTexture(_1280x720_texture);

	if (!auto_detection_texture.loadFromFile("resources\\textures\\menu\\auto-detection.png")) {
		throw exception("Error: texture of auto-detection button is not loaded");
	}
	auto_detection_sprite.setTexture(auto_detection_texture);

	if (!full_screen_texture.loadFromFile("resources\\textures\\menu\\full_screen.png")) {
		throw exception("Error: texture of full_screen button is not loaded");
	}
	full_screen_sprite.setTexture(full_screen_texture);
}