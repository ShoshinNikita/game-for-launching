// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <sstream>
#include <iostream>
#include <fstream>
#include <clocale>

#include "tinyxml2.h"

#include "Map.h"
#include "Player.h"
#include "Global.h"
#include "Message.h"
#include "View.h"

using namespace tinyxml2;
using namespace std;
using namespace sf;

////// Level
bool Level::load_from_file(string filename)
{
	setlocale(LC_ALL, "Russian");
	XMLDocument level_file;

	// Load map (xml-file)

	if (level_file.LoadFile(filename.c_str())) {
		cout << "Error: level_file (" << filename << ") is not loaded\n\n";
		return false;
	}

	// Work with container 'map'
	XMLElement *map;

	if (level_file.FirstChildElement("map")) {
		map = level_file.FirstChildElement("map");
	}
	else {
		cout << "Error: bad .tmx file (there is no \"map\")\n\n";
		return false;
	}

	width = atoi(map->Attribute("width"));
	height = atoi(map->Attribute("height"));

	// Definion of tileset and first tile
	XMLElement *tile_set_element;
	tile_set_element = map->FirstChildElement("tileset");

	while (tile_set_element) {
		Tile_set temp_tile_set;

		temp_tile_set.tile_width = atoi(tile_set_element->Attribute("tilewidth"));
		temp_tile_set.tile_height = atoi(tile_set_element->Attribute("tileheight"));
		temp_tile_set.first_tile_id = atoi(tile_set_element->Attribute("firstgid"));
		temp_tile_set.last_tile_id = atoi(tile_set_element->Attribute("tilecount"));

		// source - path to the tileset
		XMLElement *picture;
		picture = tile_set_element->FirstChildElement("image");
		string image_path = picture->Attribute("source");
		image_path.erase(image_path.begin(), image_path.begin() + 3);
		Image tileset;
		if (!tileset.loadFromFile(image_path)) {
			cout << "Error: can't load tileset (" << image_path << ")\n\n";
			return false;
		}

		temp_tile_set.tileset_image.loadFromImage(tileset);
		temp_tile_set.tileset_image.setSmooth(false);

		int columns = temp_tile_set.tileset_image.getSize().x / temp_tile_set.tile_width;
		int rows = temp_tile_set.tileset_image.getSize().y / temp_tile_set.tile_height;

		// Vector with rectangles of images
		for (int y = 0; y < rows; y++) {
			for (int x = 0; x < columns; x++) {
				Rect<int> temp_rect;

				temp_rect.top = y * temp_tile_set.tile_height;
				temp_rect.left = x * temp_tile_set.tile_width;
				temp_rect.height = temp_tile_set.tile_height;
				temp_rect.width = temp_tile_set.tile_width;

				temp_tile_set.rectangles.push_back(temp_rect);
			}
		}

		tilesets.push_back(temp_tile_set);

		tile_set_element = tile_set_element->NextSiblingElement("tileset");
	}

	
	// Work with layers
	XMLElement *layer_element;
	if (map->FirstChildElement("layer")) {
		layer_element = map->FirstChildElement("layer");
	}
	else {
		cout << "Error: there is no layer\n\n";
		return false;
	}

	while (layer_element) {
		Layer layer;
		layer.name = layer_element->Attribute("name");

		bool is_block_layer = false;
		bool is_thorns_layer = false;
		bool is_stairs_layer = false;
		bool is_half_blocks_layer = false;

		if (layer.name == "blocks") {
			is_block_layer = true;
		}
		else if (layer.name == "thorns") {
			is_thorns_layer = true;
		}
		else if (layer.name == "stairs") {
			is_stairs_layer = true;
		}
		else if (layer.name == "half-blocks") {
			is_half_blocks_layer = true;
		}
		
		
		if (layer_element->Attribute("opacity") != NULL) {
			string temp = layer_element->Attribute("opacity");
			if (temp.find('.') != string::npos) {
				temp[temp.find('.')] = ',';
			}
			double opacity = stod(temp);

			layer.opasity = 255 * opacity;
		}
		else {
			layer.opasity = 255;
		}

		XMLElement *Layer_data_element;
		Layer_data_element = layer_element->FirstChildElement("data");
		if (Layer_data_element == NULL) {
			cout << "Bad map. No layer information found.\n\n";
			return false;
		}

		string tiles = Layer_data_element->GetText();

		istringstream s_tiles(tiles);

		int x = 0, y = 0;
		for (int i = 0; i < width*height; i++) {

			int tile_GID;
			s_tiles >> tile_GID;
			s_tiles.ignore();
			int sub_rect_to_use = tile_GID - tilesets[0].first_tile_id;

			if (sub_rect_to_use < tilesets[0].last_tile_id && sub_rect_to_use >= 0) {
				Sprite sprite;
				sprite.setTexture(tilesets[0].tileset_image);
				sprite.setTextureRect(tilesets[0].rectangles[sub_rect_to_use]);

				sprite.setPosition(x*tilesets[0].tile_width, y * tilesets[0].tile_height);
				sprite.setColor(Color(255, 255, 255, layer.opasity));

				layer.tiles.push_back(sprite);

				if (is_block_layer) {
					Object temp_obj;
					temp_obj.name = "block";
					temp_obj.sprite = sprite;
					RectangleShape temp_rect;
					temp_rect.setPosition(sprite.getPosition());
					temp_rect.setSize(Vector2f(tilesets[0].tile_width, tilesets[0].tile_height));

					temp_obj.rectangle = temp_rect;

					blocks.push_back(temp_obj);
				}
				if (is_thorns_layer) {
					Object temp_obj;
					temp_obj.name = "thorn";
					temp_obj.sprite = sprite;

					RectangleShape temp_rect;
					temp_rect.setPosition(sprite.getPosition());
					temp_rect.setSize(Vector2f(tilesets[0].tile_width, tilesets[0].tile_height));

					temp_obj.rectangle = temp_rect;

					thorns.push_back(temp_obj);
				}
				if (is_stairs_layer) {
					Object temp_obj;
					temp_obj.name = "stairs";
					temp_obj.sprite = sprite;

					RectangleShape temp_rect;
					temp_rect.setPosition(sprite.getPosition());
					temp_rect.setSize(Vector2f(tilesets[0].tile_width, tilesets[0].tile_height));

					temp_obj.rectangle = temp_rect;

					stairs.push_back(temp_obj);
				}
				if (is_half_blocks_layer) {
					Object temp_obj;
					temp_obj.name = "half_block";
					temp_obj.sprite = sprite;

					RectangleShape temp_rect;
					temp_rect.setPosition(sprite.getPosition());
					temp_rect.setSize(Vector2f(tilesets[0].tile_width, tilesets[0].tile_height));

					temp_obj.rectangle = temp_rect;

					half_blocks.push_back(temp_obj);
				}
			}
			else if (sub_rect_to_use >= 0) {
				sub_rect_to_use += tilesets[0].first_tile_id;

				for (int j = 1; j < tilesets.size(); j++) {

					if (sub_rect_to_use < tilesets[j].first_tile_id + tilesets[j].last_tile_id) {
						Sprite sprite;
						sprite.setTexture(tilesets[j].tileset_image);
						sprite.setTextureRect(tilesets[j].rectangles[sub_rect_to_use - tilesets[j].first_tile_id]);

						sprite.setPosition(x*tilesets[0].tile_width, (y-1) * tilesets[0].tile_height);
						sprite.setColor(Color(255, 255, 255, layer.opasity));

						layer.tiles.push_back(sprite);

						if (is_block_layer) {
							Object temp_obj;
							temp_obj.name = "block";
							temp_obj.sprite = sprite;
							RectangleShape temp_rect;
							temp_rect.setPosition(sprite.getPosition());
							temp_rect.setSize(Vector2f(tilesets[j].tile_width, tilesets[j].tile_height));

							temp_obj.rectangle = temp_rect;

							blocks.push_back(temp_obj);
						}
						if (is_thorns_layer) {
							Object temp_obj;
							temp_obj.name = "thorn";
							temp_obj.sprite = sprite;

							RectangleShape temp_rect;
							temp_rect.setPosition(sprite.getPosition());
							temp_rect.setSize(Vector2f(tilesets[j].tile_width, tilesets[j].tile_height));

							temp_obj.rectangle = temp_rect;

							thorns.push_back(temp_obj);
						}
						if (is_stairs_layer) {
							Object temp_obj;
							temp_obj.name = "stairs";
							temp_obj.sprite = sprite;

							RectangleShape temp_rect;
							temp_rect.setPosition(sprite.getPosition());
							temp_rect.setSize(Vector2f(tilesets[j].tile_width, tilesets[j].tile_height));

							temp_obj.rectangle = temp_rect;

							stairs.push_back(temp_obj);
						}
						if (is_half_blocks_layer) {
							Object temp_obj;
							temp_obj.name = "half_block";
							temp_obj.sprite = sprite;

							RectangleShape temp_rect;
							temp_rect.setPosition(sprite.getPosition());
							temp_rect.setSize(Vector2f(tilesets[j].tile_width, tilesets[j].tile_height));

							temp_obj.rectangle = temp_rect;

							half_blocks.push_back(temp_obj);
						}

						break;
					}
				}
			}
			x++;
			if (x >= width) {
				x = 0;
				y++;
				if (y >= height) {
					y = 0;
				}
			}
		}

		layers.push_back(layer);
		layer_element = layer_element->NextSiblingElement("layer");
	}

	
	// Work with Objects
	XMLElement *object_group_element;
	if (map->FirstChildElement("objectgroup") != NULL) {
		object_group_element = map->FirstChildElement("objectgroup");

		while (object_group_element) {

			bool is_enemies_object_layer = false;
			bool is_messages_layer = false;

			if (object_group_element->Attribute("name") != NULL) {
				string temp_name = object_group_element->Attribute("name");
				if (temp_name == "enemies") {
					is_enemies_object_layer = true;
				}
				else if (temp_name == "messages") {
					is_messages_layer = true;
				}
			}

			XMLElement *object_element;
			object_element = object_group_element->FirstChildElement("object");

			while (object_element) {
				string object_type;
				if (is_enemies_object_layer) {
					object_type = "enemy";
				}
				else if (is_messages_layer) {
					object_type = "text";
				}
				else if (object_element->Attribute("type") != NULL) {
					object_type = object_element->Attribute("type");
				}

				string object_name;
				if (object_element->Attribute("name") != NULL) {
					object_name = object_element->Attribute("name");
				}
				int id = atoi(object_element->Attribute("gid"));
				int x = atoi(object_element->Attribute("x"));
				int y = atoi(object_element->Attribute("y"));

				int obj_width = atoi(object_element->Attribute("width"));
				int obj_height = atoi(object_element->Attribute("height"));

				y -= obj_height;

				Sprite sprite;

				if (id < tilesets[0].last_tile_id) {
					sprite.setTexture(tilesets[0].tileset_image);
					sprite.setTextureRect(IntRect(0, 0, 0, 0));
					sprite.setPosition(x, y);
					sprite.setTextureRect(tilesets[0].rectangles[atoi(object_element->Attribute("gid")) - tilesets[0].first_tile_id]);

				}
				else {
					for (int j = 1; j < tilesets.size(); j++) {

						if (id < tilesets[j].first_tile_id + tilesets[j].last_tile_id) {
							sprite.setTexture(tilesets[j].tileset_image);
							sprite.setTextureRect(IntRect(0, 0, 0, 0));
							sprite.setPosition(x, y);
							sprite.setTextureRect(tilesets[j].rectangles[atoi(object_element->Attribute("gid")) - tilesets[j].first_tile_id]);

							break;
						}
					}
				}

				Object object;
				object.name = object_name;
				object.type = object_type;
				object.sprite = sprite;

				RectangleShape object_rect;
				object_rect.setPosition(x, y);
				object_rect.setSize(Vector2f(obj_width, obj_height));
				object.rectangle = object_rect;

				XMLElement *properties;
				properties = object_element->FirstChildElement("properties");
				if (properties != NULL) {
					XMLElement *prop;
					prop = properties->FirstChildElement("property");

					if (prop != NULL) {
						while (prop) {
							string property_name = prop->Attribute("name");
							string property_value = prop->Attribute("value");

							object.properties[property_name] = property_value;

							prop = prop->NextSiblingElement("property");
						}
					}
				}

				objects.push_back(object);
				object_element = object_element->NextSiblingElement("object");
			}

			object_group_element = object_group_element->NextSiblingElement("objectgroup");
		}
	}
	else {
		cout << "No object layers found\n\n";
	}
	
	// Background
	XMLElement *XML_background;
	if (map->FirstChildElement("imagelayer")) {
		XML_background = map->FirstChildElement("imagelayer");

		float offset_x = 0, offset_y = 0;
		if (XML_background->Attribute("offsetx")) {
			offset_x = strtod(XML_background->Attribute("offsetx"), NULL);
		}
		if (XML_background->Attribute("offsety")) {
			offset_y = strtod(XML_background->Attribute("offsety"), NULL);
		}
		string background_path = XML_background->FirstChildElement("image")->Attribute("source");
		background_path.erase(background_path.begin(), background_path.begin() + 3);
		background_texture.loadFromFile(background_path);
		background_texture.setSmooth(false);
		background_sprite.setTexture(background_texture);
		background_sprite.setPosition(offset_x, offset_y);
	}
	else {
		cout << "Error: there is no background image\n\n";
		//return false;
	}

	return true;
}

void Level::draw(RenderWindow &window, Player& player)
{
	bool was_player_drawed = false;
	bool was_enemies_drawed = false;
	bool was_objects_draw = false;

	window.draw(background_sprite);
	for (int l = 0; l < layers.size(); l++) {
		if (layers[l].name == "for_player_drawing") {
			// TODO: for debug
			window.draw(player.get_rectangle());
			window.draw(player.get_sprite());
			was_player_drawed = true;
		}
		if (layers[l].name == "for_enemies_drawing") {
			was_enemies_drawed = true;
			for (int i = 0; i < enemies.size(); i++) {
				if (enemies[i].is_alive()) {
					// TODO: For debug
					window.draw(enemies[i].get_attack_line());
					window.draw(enemies[i].get_rectangle());

					enemies[i].draw(window);
				}
			}
		}
		if (layers[l].name == "for_objects_drawing") {
			was_objects_draw = true;
			for (int i = 0; i < messages_vector.size(); i++) {
				window.draw(messages_vector[i].get_sprite());
			}
		}

		for (int t = 0; t < layers[l].tiles.size(); t++) {
			window.draw(layers[l].tiles[t]);
		}
	}

	if (!was_player_drawed) {
		// TODO: for debug
		window.draw(player.get_rectangle());
		window.draw(player.get_sprite());
	}
	if (!was_enemies_drawed) {
		for (int i = 0; i < enemies.size(); i++) {
			if (enemies[i].is_alive()) {
				// For debug
				window.draw(enemies[i].get_attack_line());
				window.draw(enemies[i].get_rectangle());

				enemies[i].draw(window);
			}
		}
	}
	if (!was_objects_draw) {
		for (int i = 0; i < messages_vector.size(); i++) {
			window.draw(messages_vector[i].get_sprite());
		}
	}

	// Projectiles
	for (int i = 0; i < projectiles.size(); i++) {
		projectiles[i].draw(window);
	}
	// Hitscans
	for (int i = 0; i < hitscans.size(); i++) {
		hitscans[i].draw(window);
	}
	// Messages (text)
	for (int i = 0; i < messages_vector.size(); i++) {
		messages_vector[i].show(window);
	}

	// TODO: Where to draw?
	window.draw(finish.sprite);

	// Draw life
	for (int i = 0; i < MAX_HP; i++) {
		RectangleShape temp_rect;
		temp_rect.setPosition(view.getCenter().x - view.getSize().x / 2
			+ i * (full_health_sprite.getTextureRect().width + 10) + 10,
			10 + view.getCenter().y - view.getSize().y / 2);

		if (i < player.get_hp()) {
			full_health_sprite.setPosition(temp_rect.getPosition());
			window.draw(full_health_sprite);
		}
		else {
			empty_health_sprite.setPosition(temp_rect.getPosition());
			window.draw(empty_health_sprite);
		}

	}
	// Draw mana
	for (int i = 0; i < MAX_MANA; i++) {
		RectangleShape temp_rect;
		temp_rect.setPosition(view.getCenter().x - view.getSize().x / 2
			+ i * (full_mana_sprite.getTextureRect().width + 10) + 10,
			10 + view.getCenter().y - view.getSize().y / 2 + full_health_sprite.getTextureRect().height * 1.5);

		if (i < player.get_mana()) {
			full_mana_sprite.setPosition(temp_rect.getPosition());
			window.draw(full_mana_sprite);
		}
		else {
			empty_mana_sprite.setPosition(temp_rect.getPosition());
			window.draw(empty_mana_sprite);
		}
	}
}

void Level::set_config(string filename)
{
	ifstream input(filename);
	string temp;

	while (getline(input, temp)) {
		level_paths.push_back(temp);
	}

	input.close();
}

void Level::load_level(int number) {
	if (number < level_paths.size()) {

		level_number = number;

		// Level class
		tilesets.clear();
		layers.clear();
		objects.clear();

		// Global.h
		blocks.clear();
		half_blocks.clear();
		stairs.clear();
		projectiles.clear();
		hitscans.clear();
		thorns.clear();
		enemies.clear();
		messages_vector.clear();

		if (!load_from_file(level_paths[level_number])) {
			cout << "Error: can't load level.\n\nPress Enter to close the window...";
			cin.ignore();
			exit(1);
		}

		vector<Object> temp_vect = get_objects("enemy");
		for (int i = 0; i < temp_vect.size(); i++) {
			Enemy temp_enemy(temp_vect[i]);
			enemies.push_back(temp_enemy);
		}
		temp_vect.clear();


		temp_vect = get_objects("text");
		for (int i = 0; i < temp_vect.size(); i++) {
			Message temp_message(temp_vect[i]);
			messages_vector.push_back(temp_message);
		}
		temp_vect.clear();

		spawn = get_object("spawn");
		finish = get_object("finish");
	}
}

void Level::next_level()
{
	level_number++;
	load_level(level_number);
}

void Level::reload_level() {
	load_level(saved_number_level);
}

void Level::move_background(View& view)
{
	background_sprite.setPosition(view.getCenter().x - view.getSize().x / 2,
		view.getCenter().y - view.getSize().y / 2);
}

int Level::get_level_number()
{
	return level_number;
}

Object Level::get_object(string name)
{
	for (int i = 0; i < objects.size(); i++) {
		if (objects[i].name == name) {
			return objects[i];
		}
	}

	cout << "Error: bad map (there is no \"" << name << "\")\n\n";
	cout << "Please press the Enter";
	cin.ignore();
	exit(1);
}

vector<Object> Level::get_objects(string type)
{
	vector<Object> temp;
	for (int i = 0; i < objects.size(); i++) {
		if (objects[i].type == type) {
			temp.push_back(objects[i]);
		}
	}
	return temp;
}

Vector2f Level::get_level_size() 
{
	return Vector2f(width, height);
}

bool Level::is_end() {
	if (level_number + 1 >= level_paths.size()) {
		return true;
	}
	else {
		return false;
	}
}
////// Level End