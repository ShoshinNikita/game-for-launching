#ifndef MAP_H
#define MAP_H

#include <string>
#include <vector>
#include <map>

#include "SFML\Graphics.hpp"

#include "Player.h"

using namespace std;
using namespace sf;

struct Object
{
	string name;
	string type;
	RectangleShape rectangle;
	map<string, string> properties;
	Sprite sprite;
};

struct Layer
{
	string name;
	int opasity;
	vector<Sprite> tiles;
};

class Level
{
	int width, height;
	struct Tile_set
	{
		int tile_width, tile_height;
		int first_tile_id;
		int last_tile_id;
		Texture tileset_image;

		vector<Rect<int>> rectangles;
	};
	
	vector<Tile_set> tilesets;

	vector<Object> objects;
	vector<Layer> layers;
	Texture background_texture;
	Sprite background_sprite;

	vector<string> level_paths;
	int level_number;

public:
	Level() : level_number(0)
	{ }

	///<summary>Draw level (all) + player</summary>
	void draw(RenderWindow&, Player&);

	void set_config(string);
	bool load_from_file(string);

	void load_level(int);
	void next_level();
	void reload_level();

	void move_background(View&);

	int get_level_number();

	/// <summary> Return Object with according name </summary>
	Object get_object(string);

	/// <summary> Return Objects with according type </summary>
	vector<Object> get_objects(string);

	/// <summary> Return true if not last level, else - false </summary>
	
	Vector2f get_level_size();

	bool is_end();
};

#endif // !MAP_H

