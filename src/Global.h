#ifndef GLOBAL_H
#define GLOBAL_H

#include "SFML\Graphics.hpp"

#include <string>
#include <vector>

#include "Magic.h"
#include "Map.h"
#include "Enemy.h"
#include "Message.h"


using namespace std;
using namespace sf;

struct saved_enemy_stats
{
	int hp;
	Vector2f position;
};

// Developer tools
extern int developer_number_of_level;
//

/// in Pixels
extern const float MAX_MESSAGE_SIZE;
extern const int MESSAGE_FONT_SIZE;
enum player_choice{continue_game, new_game, exit_from_game};
extern const Vector2f visible_area;

// For loading saves
extern int saved_number_level;
extern int saved_hp;
extern int saved_mana;
extern Vector2f saved_character_position;

// Main constants
extern const float FPS;
extern const int gravity;
extern const Time MAX_IMMORTAL_TIME;
extern const Vector2i TILE_SIZE;
extern const float SPEED;

// Game constants
enum Player_attacks{projectile_attack, hitscan_attack};
enum Enemy_attacks {projetile, melee};

// Projectiles
extern const int PROJECTILE_DAMAGE;
extern const float MAX_PROJECTILE_SPEED;
extern const Time PROJECTILE_PLAYER_DELAY;
extern const Time PROJECTILE_ENEMY_DELAY;
extern const float MAX_PROJECTILE_RANGE;
extern const int PROJECTILE_MANA_COST;
// Hitscans
extern const int HITSCAN_DAMAGE;
extern const Time HITSCAN_PLAYER_DELAY;
extern const Time HITSCAN_ENEMY_DELAY;
extern const int HITSCAN_MANA_COST;
// For jump
extern const float MAX_TIME_IN_AIR;
// Stats
extern const int MAX_HP;
extern const int MAX_MANA;

// Gloval vector's
extern vector<Object> blocks;
extern vector<Object> half_blocks;
extern vector<Projectile> projectiles;
extern vector<Hitscan> hitscans;
extern vector<Object> thorns;
extern vector<Object> stairs;
extern vector<Enemy> enemies;
extern vector<saved_enemy_stats> saved_enemies_stats;
extern vector<Message> messages_vector;
extern vector<string> dialogs;

// Main Object's
extern Object spawn;
extern Object finish;

// Const string's
extern const string UP;
extern const string DOWN;
extern const string RIGHT;
extern const string LEFT;
extern const string WAITING;
extern const string ENEMY_OWNER;
extern const string PLAYER_OWNER;

// Textures and sprites
extern Texture hero_texture;
extern Sprite hero_sprite;

extern Texture project_texture;
extern Sprite project_sprite;

extern Texture hitscan_texture;
extern Sprite hitscan_sprite;

extern Texture full_health_texture;
extern Sprite full_health_sprite;

extern Texture empty_health_texture;
extern Sprite empty_health_sprite;

extern Texture full_mana_texture;
extern Sprite full_mana_sprite;

extern Texture empty_mana_texture;
extern Sprite empty_mana_sprite;

// Font
extern Font ArialRegular;

wstring to_wstring(string);

// Textures for main menu
extern Texture background_texture;
extern Sprite background_sprite;
extern Texture continue_texture;
extern Sprite continue_sprite;
extern Texture new_game_texture;
extern Sprite new_game_sprite;
extern Texture settings_texture;
extern Sprite settings_sprite;
extern Texture exit_texture;
extern Sprite exit_sprite;

// Textures for pause menu
extern Texture pause_texture;
extern Sprite pause_sprite;
extern Texture save_texture;
extern Sprite save_sprite;

// Textures for spell menu
extern Texture resolution_texture;
extern Sprite resolution_sprite;
extern Texture _1920x1080_texture;
extern Sprite _1920x1080_sprite;
extern Texture _1600x900_texture;
extern Sprite _1600x900_sprite;
extern Texture _1366x768_texture;
extern Sprite _1366x768_sprite;
extern Texture _1280x720_texture;
extern Sprite _1280x720_sprite;
extern Texture auto_detection_texture;
extern Sprite auto_detection_sprite;
extern Texture full_screen_texture;
extern Sprite full_screen_sprite;
#endif