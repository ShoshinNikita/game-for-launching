// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "SFML\Graphics.hpp"

#include <string>
#include <sstream>
#include <iostream>

#include "Message.h"
#include "Global.h"
#include "Map.h"
#include "View.h"

using namespace std;
using namespace sf;

Message::Message(Object obj) {
	sprite = obj.sprite;
	shape = obj.rectangle;

	istringstream dialogs_numbers(obj.name);
	vector<int> temp_vector;
	while (dialogs_numbers.peek() != EOF) {
		int r;
		dialogs_numbers >> r;
		dialogs_numbers.ignore();
		temp_vector.push_back(r);
	}

	for (int i = 0; i < temp_vector.size(); i++) {
		int number_of_linens = 1;

		Text temp_text;
		RectangleShape temp_rect;
		temp_rect.setFillColor(Color::Black);

		string temp_string;
		string str_for_cut = dialogs[temp_vector[i] - 1];

		// Designing the text


		while (MAX_MESSAGE_SIZE < [str_for_cut]() ->int { 
									int Sum = 0;
									for (int p = 0; p < str_for_cut.size(); p++) {
										if (str_for_cut[p] == ' ') {
											Sum += MESSAGE_FONT_SIZE / 2;
										}
										else {
											Sum += ArialRegular.getGlyph(str_for_cut[p], MESSAGE_FONT_SIZE, false).textureRect.width;
										}
									}
									return Sum; }()
		) {

			for (int j = str_for_cut.size() - 1; j >= 0; j--) {
				if (str_for_cut.size() - j <= MAX_MESSAGE_SIZE && str_for_cut[j] == ' ') {
					str_for_cut[j] = '\n';
					temp_string.append(str_for_cut.begin(), str_for_cut.begin() + j);
					str_for_cut.erase(str_for_cut.begin(), str_for_cut.begin() + j);
					
					number_of_linens++;
					
					break;
				}
			}

		}
		if (!str_for_cut.empty()) {
			temp_string.append(str_for_cut);
		}

		temp_rect.setSize(Vector2f(MAX_MESSAGE_SIZE, ArialRegular.getLineSpacing(MESSAGE_FONT_SIZE) * number_of_linens));

		temp_text.setString(temp_string);
		temp_text.setFont(ArialRegular);
		temp_text.setCharacterSize(MESSAGE_FONT_SIZE);
		
		// Set position
		temp_text.setPosition(shape.getPosition().x, shape.getPosition().y - ArialRegular.getLineSpacing(MESSAGE_FONT_SIZE) * number_of_linens - 10);
		temp_rect.setPosition(shape.getPosition().x, shape.getPosition().y - ArialRegular.getLineSpacing(MESSAGE_FONT_SIZE) * number_of_linens - 10);

		replics.push_back(temp_text);
		message_mackground.push_back(temp_rect);
	}

	time = seconds(1);
	position = obj.rectangle.getPosition();
	position.y += 10;

	counter = -1;
	visible = false;
}

// Change
void Message::show(RenderWindow& window) {
	if (visible) {
		window.draw(message_mackground[counter]);
		//replics[counter].move(0, -0.5);
		window.draw(replics[counter]);
	}
}

void Message::set_visible(bool vis) {
	time = timer.getElapsedTime();
	if (time.asSeconds() >= seconds(0.5).asSeconds() && vis) {

		if (counter + 1 >= replics.size()) {
			counter = -1;
		}
		visible = vis;
		counter++;
		timer.restart();
	}
	else {
		visible = vis;
	}
}

bool Message::is_showing() {
	return visible;
}

Sprite Message::get_sprite() {
	return sprite;
}

RectangleShape Message::get_rectangle() {
	return shape;
}