#ifndef LIVE_OBJECTS_H
#define LIVE_OBJECTS_H

#include "SFML\Graphics.hpp"
using namespace std;
using namespace sf;

class Live_objects
{
protected:
	int hp, mana;
	int width, height;
	int rect_w, rect_h;

	RectangleShape rectangle;

	// Sprites
	Sprite sprite;
	vector<IntRect> running_sprites;
	vector<IntRect> death_sprites;
	vector<IntRect> waiting_sprites;
	vector<IntRect> jumping_sprites;
	vector<IntRect> falling_sprites;
	vector<IntRect> double_jumping_sprites;

	// For moving treatment
	string x_direction;
	string y_direction;
	float speed;
	float boost;
	vector<string> commands;

	// For changing sprites
	float current_frame;

	Color color;

	bool on_block;
	/// For setting sprite in case hero isn't doing something
	bool is_waiting;
	
	// Functions
	///<summary> Return Vector2i for move !!!(Possible errors if gravity is float)</summary>
	Vector2i move();

	void load_sprite_rects(string);
};

#endif // !LIVE_OBJECTS_H
