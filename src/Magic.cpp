// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "SFML\Graphics.hpp"

#include "Magic.h"
#include <vector>
#include <string>
#include "Map.h"
#include "Global.h"
#include <iostream>
#include "Player.h"

using namespace std;
using namespace sf;


// Attack_skills
void Attack_skills::draw(RenderWindow& window) 
{
	window.draw(sprite);

}

bool Attack_skills::can_be_damaged(string targ, Time delay_between_damaging)
{
	for (int i = 0; i < targets.size(); i++) {
		if (targ == targets[i].target) {
			targets[i].time = targets[i].timer.getElapsedTime();
			if (targets[i].time >= delay_between_damaging || targets[i].is_first_time) {
				targets[i].timer.restart();
				targets[i].is_first_time = false;
				return true;
			}
			else {
				return false;
			}
		}
	}
}

Sprite Attack_skills::get_sprite()
{
	return sprite;
}

string Attack_skills::get_owner()
{
	return owner;
}

int Attack_skills::get_mana_cost() {
	return mana_cost;
}

float Attack_skills::get_range() {
	return range;
}


// Projectile
Projectile::Projectile(string temp_name, Sprite spr, string direction, RectangleShape& shape, string host) {
	name = temp_name;
	// TODO


	done_distance = 0;

	vector_direction.x = 10;
	vector_direction.y = 0;

	range = MAX_PROJECTILE_RANGE;
	
	damage = PROJECTILE_DAMAGE;
	
	sprite = spr;
	owner = host;

	mana_cost = PROJECTILE_MANA_COST;

	if (direction == RIGHT) {
		sprite.setPosition(shape.getPosition().x + shape.getSize().x / 2, shape.getPosition().y - sprite.getTextureRect().height);
	}
	else if (direction == LEFT) {
		IntRect temp_rect = sprite.getTextureRect();
		temp_rect.left = temp_rect.left + temp_rect.width;
		temp_rect.width *= -1;
		sprite.setTextureRect(temp_rect);

		sprite.setPosition(shape.getPosition().x - shape.getSize().x / 2, shape.getPosition().y - sprite.getTextureRect().height);
		vector_direction.x *= -1;
	}

	rectangle.setPosition(sprite.getPosition());
	// Change!!!
	rectangle.setSize(Vector2f(abs(sprite.getTextureRect().width), sprite.getTextureRect().height));
	
	// For player, Change
	sprite.setColor(Color::Blue);

	is_drawable = true;
}

Projectile::Projectile(string temp_name, Sprite spr, RectangleShape& shape, string host, Vector2f vect, int dmg) {
	name = temp_name;
	// TODO

	
	done_distance = 0;

	vector_direction = vect;

	range = MAX_PROJECTILE_RANGE;
	damage = dmg;
	sprite = spr;
	owner = host;

	if (vect.x < 0) {
		sprite.setPosition(shape.getPosition().x + shape.getSize().x / 2, shape.getPosition().y - sprite.getTextureRect().height);
	}
	else {
		IntRect temp_rect = sprite.getTextureRect();
		temp_rect.left = temp_rect.left + temp_rect.width;
		temp_rect.width *= -1;
		sprite.setTextureRect(temp_rect);

		sprite.setPosition(shape.getPosition().x - shape.getSize().x / 2, shape.getPosition().y - sprite.getTextureRect().height);
	}

	rectangle.setPosition(sprite.getPosition());

	rectangle.setSize(Vector2f(abs(sprite.getTextureRect().width), sprite.getTextureRect().height));

	is_drawable = true;
}

void Projectile::move(Player& player) {
	bool intersection = false;

	if (done_distance < MAX_PROJECTILE_RANGE && is_drawable) {
		for (auto i : blocks) {
			if (rectangle.getGlobalBounds().intersects(i.rectangle.getGlobalBounds())) {
				intersection = true;
				is_drawable = false;
			}
		}

		if (!intersection) {
			for (int i = 0; i < enemies.size(); i++) {
				if (rectangle.getGlobalBounds().intersects(enemies[i].get_rectangle().getGlobalBounds())) {
					if (owner == PLAYER_OWNER && enemies[i].is_alive()) {
						enemies[i].damage_enemy(damage);
						intersection = true;
						is_drawable = false;
					}
				}
			}
		}

		if (!intersection) {
			if (rectangle.getGlobalBounds().intersects(player.get_rectangle().getGlobalBounds())) {
				if (owner == ENEMY_OWNER) {
					player.damage_hero(damage);
					intersection = true;
					is_drawable = false;
				}
			}
		}

		if (!intersection) {
			done_distance += sqrt(vector_direction.x*vector_direction.x + vector_direction.y*vector_direction.y);
			rectangle.move(vector_direction);
		}
	}
	else {
		intersection = true;
		is_drawable = false;
	}
	sprite.setPosition(rectangle.getPosition());
}

void Projectile::draw(RenderWindow& window) 
{
	if (is_drawable) {
		window.draw(sprite);
	}
}


// Hitscan
Hitscan::Hitscan(string temp_name, Sprite spr, string direction, RectangleShape& shape, string host)
{
	name = temp_name;
	// TODO



	host_rectangle = shape;
	sprite = spr;
	x_direction = direction;
	owner = host;

	is_drawable = true;
	
	damage = HITSCAN_DAMAGE;
	range = spr.getTextureRect().width;

	mana_cost = HITSCAN_MANA_COST;

	live_time = seconds(0);

	if (x_direction == RIGHT) {
		sprite.setPosition(shape.getPosition().x + shape.getSize().x / 2, shape.getPosition().y - sprite.getTextureRect().height);
	}
	else if (x_direction == LEFT) {
		IntRect temp_rect = sprite.getTextureRect();
		temp_rect.left = temp_rect.left + temp_rect.width;
		temp_rect.width *= -1;
		sprite.setTextureRect(temp_rect);

		sprite.setPosition(shape.getPosition().x - shape.getSize().x / 2 - abs(sprite.getTextureRect().width),
							shape.getPosition().y - sprite.getTextureRect().height);
	}

	rectangle.setPosition(sprite.getPosition());
	// Change!!!
	rectangle.setSize(Vector2f(abs(sprite.getTextureRect().width), sprite.getTextureRect().height));
}

Hitscan::Hitscan(string temp_name, Sprite spr, string direction, RectangleShape& shape, string host, int dmg)
{
	name = temp_name;
	// TODO


	host_rectangle = shape;
	sprite = spr;
	x_direction = direction;
	owner = host;

	is_drawable = true;

	damage = dmg;
	range = spr.getTextureRect().width;

	mana_cost = HITSCAN_MANA_COST;

	live_time = seconds(0);

	if (x_direction == RIGHT) {
		sprite.setPosition(shape.getPosition().x + shape.getSize().x / 2, shape.getPosition().y - sprite.getTextureRect().height);
	}
	else if (x_direction == LEFT) {
		IntRect temp_rect = sprite.getTextureRect();
		temp_rect.left = temp_rect.left + temp_rect.width;
		temp_rect.width *= -1;
		sprite.setTextureRect(temp_rect);

		sprite.setPosition(shape.getPosition().x - shape.getSize().x / 2 - abs(sprite.getTextureRect().width),
			shape.getPosition().y - sprite.getTextureRect().height);
	}

	rectangle.setPosition(sprite.getPosition());
	// Change!!!
	rectangle.setSize(Vector2f(abs(sprite.getTextureRect().width), sprite.getTextureRect().height));
}

void Hitscan::attack(Player& player)
{
	if (is_drawable) {
		if (owner == PLAYER_OWNER){
			for (int i = 0; i < enemies.size(); i++) {
				if (rectangle.getGlobalBounds().intersects(enemies[i].get_rectangle().getGlobalBounds())) {

					bool was_in_vector = false;
					for (auto j : targets) {
						if (j.target == to_string(i)) {
							was_in_vector = true;
						}
					}

					if (!was_in_vector) {
						delay_between_dmg temp;
						temp.target = to_string(i);
						temp.is_first_time = true;
						targets.push_back(temp);
					}
					//TODO
					if (enemies[i].is_alive() && can_be_damaged(to_string(i),/*This ->*/ seconds(1))) {
						enemies[i].damage_enemy(damage);
					}
					
				}
			}
		}
		else if (owner == ENEMY_OWNER) {
			if (rectangle.getGlobalBounds().intersects(player.get_rectangle().getGlobalBounds())) {

				bool was_in_vector = false;
				for (auto j : targets) {
					if (j.target == "player") {
						was_in_vector = true;
					}
				}

				if (!was_in_vector) {
					delay_between_dmg temp;
					temp.target = "player";
					temp.is_first_time = true;
					targets.push_back(temp);
				}
				// TODO
				if (can_be_damaged("player",/*This ->*/ seconds(1))) {
					player.damage_hero(damage);
				}
				
			}
		}
	}
}

void Hitscan::draw(sf::RenderWindow& window)
{
	live_time = live_timer.getElapsedTime();
	if (live_time.asSeconds() >= seconds(1).asSeconds()) {
		is_drawable = false;
	}

	if(is_drawable) {
		window.draw(sprite);
	}
}

// TODO (?)
void Hitscan::move(RectangleShape& shape)
{
	
}

void Hitscan::move(Player& player)
{
	if (owner == PLAYER_OWNER) {
		x_direction = player.get_x_direction();


		if (x_direction == RIGHT) {
			rectangle.setPosition(player.get_rectangle().getPosition().x + player.get_rectangle().getSize().x / 2,
				player.get_rectangle().getPosition().y - sprite.getTextureRect().height);
		}
		else {
			rectangle.setPosition(player.get_rectangle().getPosition().x - player.get_rectangle().getSize().x / 2 - abs(sprite.getTextureRect().width),
				player.get_rectangle().getPosition().y - sprite.getTextureRect().height);
		}

		sprite.setPosition(rectangle.getPosition());
	}
	else if (owner == ENEMY_OWNER) {
		/*
		if (x_direction == RIGHT) {
			rectangle.setPosition(host_rectangle.getPosition().x + host_rectangle.getSize().x / 2,
				host_rectangle.getPosition().y - sprite.getTextureRect().height);
		}
		else {
			rectangle.setPosition(host_rectangle.getPosition().x - host_rectangle.getSize().x / 2 - abs(sprite.getTextureRect().width),
				host_rectangle.getPosition().y - sprite.getTextureRect().height);
		}

		sprite.setPosition(rectangle.getPosition());
		*/
	}
}


// Super_attack
Super_attack::Super_attack(Sprite spr, string host)
{
	sprite = spr;
	owner = host;



	damage = 10;
	mana_cost = MAX_MANA;
}

// Super_attack End