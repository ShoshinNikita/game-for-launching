// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include"SFML\Graphics.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include "Player.h"
#include "Saves.h"
#include "Global.h"


void create_new_save_file()
{
	string comments("# File for saves\n# Example: number_of_level, character_position (x, y), life, mana.\n");
	comments.append("# New line: eneemies (hp), position (x, y) ...");
	ofstream output("saves.txt");
	output.clear();

	saved_number_level = 0;
	saved_character_position = Vector2f(0, 0);
	saved_hp = MAX_HP;
	saved_mana = MAX_MANA;

	output << comments << endl;
	output << "Player: " << saved_number_level << " " << saved_character_position.x << " " <<
		saved_character_position.y << " " << saved_hp << " " << saved_mana;

	output.close();

	saved_enemies_stats.clear();
}


void load_save_file()
{
	bool empty_file = true;
	ifstream input("saves.txt");

	while (input.peek() != EOF) {
		string temp_str;
		getline(input, temp_str);
		if (temp_str[0] != '#') {
			istringstream s_string(temp_str);
			string temp_str;

			s_string >> temp_str;

			if (temp_str == "Player:") {
				if (s_string >> saved_number_level >> saved_character_position.x >>
					saved_character_position.y >> saved_hp >> saved_mana) {
					empty_file = false;
				}
				else {
					cout << "Error: bad save file\n\n";
				}
			}
			else if (temp_str == "Enemies:") {
				saved_enemies_stats.clear();
				saved_enemy_stats temp;
				while (s_string >> temp.hp >> temp.position.x >> temp.position.y) {
					saved_enemies_stats.push_back(temp);
				}
			}
		}
	}

	input.close();

	if (empty_file) {
		create_new_save_file();
	}

}

void load_save_from_RAM(Level& level, Player& player)
{
	player.load_stats_from_save();

	level.load_level(saved_number_level);

	for (int i = 0; i < enemies.size() && i < saved_enemies_stats.size(); i++) {
		enemies[i].set_hp(saved_enemies_stats[i].hp);
		enemies[i].set_position(saved_enemies_stats[i].position);
	}

	if (saved_character_position.x != 0 && saved_character_position.y != 0 &&
		saved_character_position.x < level.get_level_size().x * TILE_SIZE.x &&
		saved_character_position.y < level.get_level_size().y * TILE_SIZE.y) {
		player.set_position(saved_character_position);
	}
	else {
		player.set_position(spawn.rectangle.getPosition());
	}
}


void save_game(Level& level, Player& player)
{
	saved_number_level = level.get_level_number();
	saved_character_position = player.get_rectangle().getPosition();
	saved_hp = player.get_hp();
	saved_mana = player.get_mana();

	string comments("# File for saves\n# Example: number_of_level, character_position (x, y), life, mana.\n");
	comments.append("# New line: eneemies (hp), position (x, y) ...");
	ofstream output("saves.txt");
	output.clear();

	// Character stats
	output << comments << endl;
	output << "Player: " << saved_number_level << " " << saved_character_position.x << " " <<
		saved_character_position.y << " " << saved_hp << " " << saved_mana << endl;

	saved_enemies_stats.clear();
	// Enemies stats (hp)
	output << "Enemies: ";
	for (auto i : enemies) {
		output << i.get_hp() << " " << i.get_rectangle().getPosition().x << " " << i.get_rectangle().getPosition().y << " ";
		saved_enemy_stats temp;
		temp.hp = i.get_hp();
		temp.position = i.get_rectangle().getPosition();
		saved_enemies_stats.push_back(temp);
	}

	output.close();
}