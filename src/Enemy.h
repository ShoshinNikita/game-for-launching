#ifndef ENEMY_H
#define ENEMY_H

#include <string>
#include <vector>

#include "SFML\Graphics.hpp"

#include "Map.h"
#include "Player.h"
#include "Live_objects.h"

class Enemy : public Live_objects
{
	int kind_of_attack;

	/// <summary> Line between player and enemy. For checking walls. </summary>
	RectangleShape attack_line;

	int damage;

	Time projectile_delay;
	Clock projectile_timer;

	void attack(Player&, RenderWindow&);

	void collision(int, int);
	void update();

public:
	
	// Interface
	void control(Player&, RenderWindow&);

	Enemy(Object);

	bool is_alive();

	void set_hp(int);
	void set_position(Vector2f);

	int get_hp();

	void damage_enemy(int);

	void draw(RenderWindow&);

	RectangleShape get_rectangle();

	/// <summary> Get line between player and enemy. For checking walls. </summary>
	RectangleShape get_attack_line();
};

#endif // !ENEMY_H