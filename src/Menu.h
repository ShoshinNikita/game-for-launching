#ifndef MENU_H
#define MENU_H
#include "Map.h"
#include "Player.h"

int show_menu_window(sf::RenderWindow&, bool);

void show_settings_menu(sf::RenderWindow&, bool&);

bool show_pause_menu(sf::RenderWindow&, bool, Level&, Player&);

void munu_of_spells(RenderWindow&, Level&, Player&);

void develop_menu(RenderWindow&, Level&, Player&);
#endif // !MENU_H

