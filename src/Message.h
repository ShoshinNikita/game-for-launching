#ifndef MESSAGE_H
#define MESSAGE_H

#include "SFML\Graphics.hpp"

#include <string>
#include <vector>

#include "Map.h"

using namespace std;
using namespace sf;

class Message
{
	RectangleShape shape;

	Sprite sprite;
	vector<Text> replics;
	vector<RectangleShape> message_mackground;

	int counter;

	bool visible;

	Vector2f position;

	Time time;
	Clock timer;

public:
	Message(Object);

	void show(RenderWindow&);
	void set_visible(bool);
	bool is_showing();
	Sprite get_sprite();
	RectangleShape get_rectangle();
};

#endif // !MESSAGE_H

