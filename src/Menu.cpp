// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "SFML\Graphics.hpp"

#include "Menu.h"
#include "Global.h"
#include "View.h"
#include "Saves.h"

using namespace std;
using namespace sf;

// For delay between menu windows
Clock timer;
const Time delay = seconds(0.3);


int show_menu_window(RenderWindow& window, bool full_screen)
{
	timer.restart();
	
	struct buttons 
	{
		enum { continue_game, new_game, settings, exit, LAST_ELEMENT };
	};
	int current_choice;
	current_choice = buttons::continue_game;

	Event event;

	continue_sprite.setPosition(50, 100);

	new_game_sprite.setPosition(50, continue_sprite.getPosition().y + continue_sprite.getTextureRect().height + 100);

	settings_sprite.setPosition(50, new_game_sprite.getPosition().y + new_game_sprite.getTextureRect().height + 100);

	exit_sprite.setPosition(50, settings_sprite.getPosition().y + settings_sprite.getTextureRect().height + 100);

	while (true) {
		if (timer.getElapsedTime() > delay) {
			Vector2f mouse_position(Mouse::getPosition(window).x, Mouse::getPosition(window).y);

			while (window.pollEvent(event)) {
				if (event.type == Event::Closed) {
					return player_choice::exit_from_game;
				}
				if (event.type == Event::KeyPressed && (event.key.code == Keyboard::S || event.key.code == Keyboard::Down)) {
					if (current_choice < buttons::LAST_ELEMENT - 1) {
						current_choice++;
					}
					else {
						current_choice = 0;
					}
				}
				if (event.type == Event::KeyPressed && (event.key.code == Keyboard::W || event.key.code == Keyboard::Up)) {
					if (current_choice > 0) {
						current_choice--;
					}
					else {
						current_choice = buttons::LAST_ELEMENT - 1;
					}
				}
			}

			if (current_choice == buttons::continue_game) {
				continue_sprite.setScale(1.5, 1.5);
				current_choice = buttons::continue_game;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					return player_choice::continue_game;
				}
			}
			else {
				continue_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::new_game) {
				new_game_sprite.setScale(1.5, 1.5);
				current_choice = buttons::new_game;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					return player_choice::new_game;
				}
			}
			else {
				new_game_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::settings) {
				settings_sprite.setScale(1.5, 1.5);
				current_choice = buttons::settings;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					show_settings_menu(window, full_screen);
				}

			}
			else {
				settings_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::exit) {
				exit_sprite.setScale(1.5, 1.5);
				current_choice = buttons::exit;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					return player_choice::exit_from_game;
				}
			}
			else {
				exit_sprite.setScale(1, 1);
			}

			window.clear();
			window.draw(background_sprite);
			window.draw(continue_sprite);
			window.draw(new_game_sprite);
			window.draw(settings_sprite);
			window.draw(exit_sprite);
			window.display();
		}
	}

}

void show_settings_menu(RenderWindow& window, bool& full_screen)
{
	timer.restart();

	struct buttons 
	{
		enum { _1920x1080, _1600x900, _1366x768, _1280x720, auto_detection, full_screen, LAST_ELEMENT };
	};
	int current_choice;
	current_choice = buttons::_1920x1080;


	int temp_x;
	
	resolution_sprite.setPosition(50, 100);

	temp_x = resolution_sprite.getPosition().x + resolution_sprite.getTextureRect().width / 2 - _1920x1080_sprite.getTextureRect().width / 2;
	_1920x1080_sprite.setPosition(temp_x, resolution_sprite.getPosition().y + resolution_sprite.getTextureRect().height + 50);

	temp_x = resolution_sprite.getPosition().x + resolution_sprite.getTextureRect().width / 2 - _1600x900_sprite.getTextureRect().width / 2;
	_1600x900_sprite.setPosition(temp_x, _1920x1080_sprite.getPosition().y + _1920x1080_sprite.getTextureRect().height + 20);

	temp_x = resolution_sprite.getPosition().x + resolution_sprite.getTextureRect().width / 2 - _1366x768_sprite.getTextureRect().width / 2;
	_1366x768_sprite.setPosition(temp_x, _1600x900_sprite.getPosition().y + _1600x900_sprite.getTextureRect().height + 20);

	temp_x = resolution_sprite.getPosition().x + resolution_sprite.getTextureRect().width / 2 - _1280x720_sprite.getTextureRect().width / 2;
	_1280x720_sprite.setPosition(temp_x, _1366x768_sprite.getPosition().y + _1366x768_sprite.getTextureRect().height + 20);

	temp_x = resolution_sprite.getPosition().x + resolution_sprite.getTextureRect().width / 2 - auto_detection_sprite.getTextureRect().width / 2;
	auto_detection_sprite.setPosition(temp_x, _1280x720_sprite.getPosition().y + _1280x720_sprite.getTextureRect().height + 20);

	full_screen_sprite.setPosition(50, auto_detection_sprite.getPosition().y + auto_detection_sprite.getTextureRect().height + 100);

	Event event;

	bool back_pressed = false;

	while (!back_pressed) {
		if (timer.getElapsedTime() > delay) {
			while (window.pollEvent(event)) {
				if (event.type == Event::Closed || event.key.code == Keyboard::Escape) {
					back_pressed = true;
				}
				if (event.type == Event::KeyPressed && (event.key.code == Keyboard::S || event.key.code == Keyboard::Down)) {
					if (current_choice < buttons::LAST_ELEMENT - 1) {
						current_choice++;
					}
					else {
						current_choice = 0;
					}
				}
				if (event.type == Event::KeyPressed && (event.key.code == Keyboard::W || event.key.code == Keyboard::Up)) {
					if (current_choice > 0) {
						current_choice--;
					}
					else {
						current_choice = buttons::LAST_ELEMENT - 1;
					}
				}
			}

			Vector2f mouse_position(Mouse::getPosition(window).x, Mouse::getPosition(window).y);

			if (current_choice == buttons::_1920x1080) {
				_1920x1080_sprite.setScale(1.2, 1.2);
				current_choice = buttons::_1920x1080;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					if (full_screen) {
						window.create(VideoMode(1920, 1080), "Game", Style::Fullscreen);
					}
					else {
						window.create(VideoMode(1920, 1080), "Game");
					}
					timer.restart();
				}
			}
			else {
				_1920x1080_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::_1600x900) {
				_1600x900_sprite.setScale(1.2, 1.2);
				current_choice = buttons::_1600x900;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					if (full_screen) {
						window.create(VideoMode(1600, 900), "Game", Style::Fullscreen);
					}
					else {
						window.create(VideoMode(1600, 900), "Game");
					}
					timer.restart();

				}
			}
			else {
				_1600x900_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::_1366x768) {
				_1366x768_sprite.setScale(1.2, 1.2);
				current_choice = buttons::_1366x768;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					if (full_screen) {
						window.create(VideoMode(1366, 768), "Game", Style::Fullscreen);
					}
					else {
						window.create(VideoMode(1366, 768), "Game");
					}
					timer.restart();
				}
			}
			else {
				_1366x768_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::_1280x720) {
				_1280x720_sprite.setScale(1.2, 1.2);
				current_choice = buttons::_1280x720;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					if (full_screen) {
						window.create(VideoMode(1280, 720), "Game", Style::Fullscreen);
					}
					else {
						window.create(VideoMode(1280, 720), "Game");
					}
					timer.restart();
				}
			}
			else {
				_1280x720_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::auto_detection) {
				auto_detection_sprite.setScale(1.2, 1.2);
				current_choice = buttons::auto_detection;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					window.create(VideoMode::getDesktopMode(), "Game");
					if (full_screen) {
						window.create(VideoMode::getDesktopMode(), "Game", Style::Fullscreen);
					}
					else {
						window.create(VideoMode::getDesktopMode(), "Game");
					}
					timer.restart();
				}
			}
			else {
				auto_detection_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::full_screen) {
				full_screen_sprite.setScale(1.2, 1.2);
				current_choice = buttons::full_screen;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					if (full_screen) {
						full_screen = false;
						window.create(VideoMode::getDesktopMode(), "Game");
					}
					else {
						full_screen = true;
						window.create(VideoMode::getDesktopMode(), "Game", Style::Fullscreen);
					}
					timer.restart();
				}
			}
			else {
				full_screen_sprite.setScale(1, 1);
			}

			view.setSize(window.getSize().x, window.getSize().y);
			view.setCenter(view.getSize().x / 2, view.getSize().y / 2);
			window.setView(view);

			window.clear();
			window.draw(background_sprite);
			window.draw(resolution_sprite);
			window.draw(_1920x1080_sprite);
			window.draw(_1600x900_sprite);
			window.draw(_1366x768_sprite);
			window.draw(_1280x720_sprite);
			window.draw(auto_detection_sprite);
			window.draw(full_screen_sprite);

			window.display();
		}
	}

	// Comments
	/*
	# Sign '#' means comment
	# Contains: Resolution, full screen mode (1 or 0) 
	# Example:
	# 1280 720 1
	#
	*/
	ifstream input("config.txt");
	vector<string> comments;
	while (input.peek() != EOF) {
		string temp_str;
		getline(input, temp_str);
		if (temp_str[0] == '#') {
			comments.push_back(temp_str);
		}
	}
	input.close();
	
	ofstream output("config.txt");
	for (auto i : comments) {
		output << i << endl;
	}
	output << window.getSize().x << " " << window.getSize().y << " " << full_screen;
	output.close();
}

bool show_pause_menu(RenderWindow& window, bool full_screen, Level& level, Player& player)
{
	timer.restart();

	struct buttons
	{
		enum { continue_game, save, settings, exit, LAST_ELEMENT };
	};
	int current_choice;
	current_choice = buttons::continue_game;

	Event event;
	
	// TODO (what is it?)
	if (window.getSize().x < view.getSize().x) {
		pause_sprite.setPosition(window.getSize().x / 2 - pause_sprite.getTextureRect().width / 2, 100);
	}
	else {
		pause_sprite.setPosition(view.getSize().x / 2 - pause_sprite.getTextureRect().width / 2, 100);
	}
	
	continue_sprite.setPosition(50, 170);

	save_sprite.setPosition(50, continue_sprite.getPosition().y + continue_sprite.getTextureRect().height + 100);

	settings_sprite.setPosition(50, save_sprite.getPosition().y + save_sprite.getTextureRect().height + 100);

	exit_sprite.setPosition(50, settings_sprite.getPosition().y + settings_sprite.getTextureRect().height + 100);

	while (true) {
		if (timer.getElapsedTime() > delay) {

			Vector2f mouse_position(Mouse::getPosition(window).x, Mouse::getPosition(window).y);

			while (window.pollEvent(event)) {
				if (event.type == Event::Closed) {
					// means exit to main menu
					return false;
				}
				if (event.type == Event::KeyPressed && (event.key.code == Keyboard::S || event.key.code == Keyboard::Down)) {
					if (current_choice < buttons::LAST_ELEMENT - 1) {
						current_choice++;
					}
					else {
						current_choice = 0;
					}
				}
				if (event.type == Event::KeyPressed && (event.key.code == Keyboard::W || event.key.code == Keyboard::Up)) {
					if (current_choice > 0) {
						current_choice--;
					}
					else {
						current_choice = buttons::LAST_ELEMENT - 1;
					}
				}
			}

			if (current_choice == buttons::continue_game) {
				continue_sprite.setScale(1.5, 1.5);
				current_choice = buttons::continue_game;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					return true;
				}
			}
			else {
				continue_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::save) {
				save_sprite.setScale(1.5, 1.5);
				current_choice = buttons::save;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					save_game(level, player);
					return true;
				}
			}
			else {
				save_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::settings) {
				settings_sprite.setScale(1.5, 1.5);
				current_choice = buttons::settings;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					show_settings_menu(window, full_screen);
				}

			}
			else {
				settings_sprite.setScale(1, 1);
			}

			if (current_choice == buttons::exit) {
				exit_sprite.setScale(1.5, 1.5);
				current_choice = buttons::exit;

				if (Keyboard::isKeyPressed(Keyboard::Return)) {
					return false;
				}
			}
			else {
				exit_sprite.setScale(1, 1);
			}

			view.setSize(window.getSize().x, window.getSize().y);
			view.setCenter(view.getSize().x / 2, view.getSize().y / 2);
			window.setView(view);

			window.clear();
			window.draw(background_sprite);
			window.draw(pause_sprite);
			window.draw(continue_sprite);
			window.draw(save_sprite);
			window.draw(settings_sprite);
			window.draw(exit_sprite);
			window.display();
		}
	}
	
}


// TODO (?)
void munu_of_spells(RenderWindow& window, Level& level, Player& player) {
	/*
	Texture projectile_button_texture;
	projectile_button_texture.loadFromFile("textures\\menu\\spell_menu\\hitscan_button.png");

	Sprite projectile_button_sprite(projectile_button_texture);

	projectile_button_sprite.setPosition(view.getCenter());
	while (true) {
		window.clear();

		level.draw(window);

		for (int i = 0; i < messages_vector.size(); i++) {
			window.draw(messages_vector[i].get_sprite());
			messages_vector[i].show(window);
		}

		for (int i = 0; i < projectiles.size(); i++) {
			projectiles[i].draw(window);
		}

		for (int i = 0; i < hitscans.size(); i++) {
			hitscans[i].draw(window);
		}

		for (int i = 0; i < enemies.size(); i++) {
			if (enemies[i].is_alive()) {
				enemies[i].draw(window);
			}

		}

		window.draw(finish.sprite);

		window.draw(player.get_sprite());

		// Draw life
		for (int i = 0; i < MAX_HP; i++) {
			RectangleShape temp_rect;
			temp_rect.setPosition(view.getCenter().x - view.getSize().x / 2
				+ i * (full_health_sprite.getTextureRect().width + 10) + 10,
				10 + view.getCenter().y - view.getSize().y / 2);

			if (i < player.get_hp()) {
				full_health_sprite.setPosition(temp_rect.getPosition());
				window.draw(full_health_sprite);
			}
			else {
				empty_health_sprite.setPosition(temp_rect.getPosition());
				window.draw(empty_health_sprite);
			}

		}

		// Draw mana
		for (int i = 0; i < MAX_MANA; i++) {
			RectangleShape temp_rect;
			temp_rect.setPosition(view.getCenter().x - view.getSize().x / 2
				+ i * (full_mana_sprite.getTextureRect().width + 10) + 10,
				10 + view.getCenter().y - view.getSize().y / 2 + full_health_sprite.getTextureRect().height * 1.5);

			if (i < player.get_mana()) {
				full_mana_sprite.setPosition(temp_rect.getPosition());
				window.draw(full_mana_sprite);
			}
			else {
				empty_mana_sprite.setPosition(temp_rect.getPosition());
				window.draw(empty_mana_sprite);
			}
		}

		window.draw(projectile_button_sprite);

		window.display();
	}
	*/
}

void develop_menu(RenderWindow& window, Level& level, Player& player)
{
	string command;
	cout << "Enter the command: ";
	cin >> command;

	if (command == "level") {
		int number;
		cin >> number;
		developer_number_of_level = number;
	}
	else {
		// TODO
	}
}

