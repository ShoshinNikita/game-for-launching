// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <vector>
#include <string>

#include "Global.h"
#include "Map.h"
#include "Message.h"

using namespace std;
using namespace sf;


int developer_number_of_level = -1;
const float MAX_MESSAGE_SIZE = 64*10;
const int MESSAGE_FONT_SIZE = 30;
const Vector2f visible_area(1600, 900);

int saved_number_level;
int saved_hp;
int saved_mana;
Vector2f saved_character_position;

const float FPS = 60;
const int gravity = 8;
const Time MAX_IMMORTAL_TIME = seconds(1.0);
const Vector2i TILE_SIZE(64, 64);
const float SPEED = 5.0;

const int PROJECTILE_DAMAGE = 1;
const float MAX_PROJECTILE_SPEED = 10;
const Time PROJECTILE_PLAYER_DELAY = seconds(0.5);
const Time PROJECTILE_ENEMY_DELAY = seconds(1.0);
const float MAX_PROJECTILE_RANGE = 64 * 15; // in pixels
const int PROJECTILE_MANA_COST = 1;

const int HITSCAN_DAMAGE = 2;
const Time HITSCAN_PLAYER_DELAY = seconds(1.5);
const Time HITSCAN_ENEMY_DELAY = seconds(2);
const int HITSCAN_MANA_COST = 0;

const float MAX_TIME_IN_AIR = gravity * 20;

const int MAX_HP = 5;
const int MAX_MANA = 5;

vector<Object> blocks;
vector<Object> half_blocks;
vector<Projectile> projectiles;
vector<Hitscan> hitscans;
vector<Object> thorns;
vector<Object> stairs;
vector<Enemy> enemies;
vector<saved_enemy_stats> saved_enemies_stats;
vector<Message> messages_vector;
vector<string> dialogs;

Object spawn;
Object finish;

const string UP = "up";
const string DOWN = "down";
const string RIGHT = "right";
const string LEFT = "left";
const string WAITING = "waiting";
const string ENEMY_OWNER = "enemy";
const string PLAYER_OWNER = "player";

Texture hero_texture;
Sprite hero_sprite;
Texture project_texture;
Sprite project_sprite;
Texture hitscan_texture;
Sprite hitscan_sprite;
Texture full_health_texture;
Sprite full_health_sprite;
Texture empty_health_texture;
Sprite empty_health_sprite;
Texture full_mana_texture;
Sprite full_mana_sprite; 
Texture empty_mana_texture;
Sprite empty_mana_sprite;

Font ArialRegular;

Texture background_texture;
Sprite background_sprite;
Texture continue_texture;
Sprite continue_sprite;
Texture new_game_texture;
Sprite new_game_sprite;
Texture settings_texture;
Sprite settings_sprite;
Texture exit_texture;
Sprite exit_sprite;

Texture pause_texture;
Sprite pause_sprite;
Texture save_texture;
Sprite save_sprite;

Texture resolution_texture;
Sprite resolution_sprite;
Texture _1920x1080_texture;
Sprite _1920x1080_sprite;
Texture _1600x900_texture;
Sprite _1600x900_sprite;
Texture _1366x768_texture;
Sprite _1366x768_sprite;
Texture _1280x720_texture;
Sprite _1280x720_sprite;
Texture auto_detection_texture;
Sprite auto_detection_sprite;
Texture full_screen_texture;
Sprite full_screen_sprite;