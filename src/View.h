#ifndef VIEW_H
#define VIEW_H

#include "SFML\Graphics.hpp"
#include "Map.h"

extern View view;
void set_camera_position(Vector2f, RenderWindow&, Level&);

#endif