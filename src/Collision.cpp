// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "SFML\Graphics.hpp"

#include <iostream>

#include "Collision.h"

using namespace std;
using namespace sf;


void project_on_axis(const Vector2f& axis, float& min, float& max, Vector2f points[4]) 
{
	// ������, ����� ������� �������!?
	min = (points[0].x*axis.x + points[0].y*axis.y);
	max = min;

	for (int i = 1; i < 4; i++) {
		int r = (points[i].x * axis.x + points[i].y * axis.y);

		if (r > max) {
			max = r;
		}
		if (r < min) {
			min = r;
		}
	}
	
}

bool is_there_intersect(RectangleShape& A, RectangleShape& B)
{
	Transform trans = A.getTransform();
	Vector2f A_rect[4];
	A_rect[0] = trans.transformPoint(0, 0);
	A_rect[1] = trans.transformPoint(0, A.getSize().y);
	A_rect[2] = trans.transformPoint(A.getSize().x, A.getSize().y);
	A_rect[3] = trans.transformPoint(A.getSize().x, 0);

	trans = B.getTransform();
	Vector2f B_rect[4];
	B_rect[0] = trans.transformPoint(0, 0);
	B_rect[1] = trans.transformPoint(0, B.getSize().y);
	B_rect[2] = trans.transformPoint(B.getSize().x, B.getSize().y);
	B_rect[3] = trans.transformPoint(B.getSize().x, 0);

	// ��� ��� ��������
	Vector2f axis[4] = {
		Vector2f(A_rect[1].x - A_rect[0].x, A_rect[1].y - A_rect[0].y),
		Vector2f(A_rect[2].x - A_rect[1].x, A_rect[2].y - A_rect[1].y),
		Vector2f(B_rect[1].x - B_rect[0].x, B_rect[1].y - B_rect[0].y),
		Vector2f(B_rect[2].x - B_rect[1].x, B_rect[2].y - B_rect[1].y)
	};

	// ����� (�����)
	for (int i = 0; i < 4; i++) {
		float A_min, A_max, B_min, B_max;

		project_on_axis(axis[i], A_min, A_max, A_rect);
		project_on_axis(axis[i], B_min, B_max, B_rect);

		if (!((B_min <= A_max) && (B_max >= A_min))) {
			return false;
		}
	}
	
	return true;
}

