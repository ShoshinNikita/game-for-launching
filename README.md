# README #

# What is it?
School-project, game - 2d-platformer. There are no special mechanics (but I hope it will change in feature).

# How to launch?
1. Press download button, which locates on left-side panel
2. Launch Game.exe

# Control
1. W,S - control in menu
2. W,A,S,D - moving the hero
3. Space - jump
4. F,V - attack
5. Z - turn on/off invisible
6. P - play music (alpha)

# Что это?
Школьный проект, игра - 2d-платформер. Никаких особенных механик пока (надеюсь в будущем это изменится) нет.

# Как запустить?
1. Нажмите кнопку Download на левой панели.
2. Запустите Game.exe

# Управление
1. W,S - управление в меню
2. W,A,S,D - управление героем
3. Space - прыжок
4. F,V - атака
5. Z - включить/выключить невидимость
6. P - проиграть музыку (alpha)